
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, SetEnvironmentVariable
from launch.substitutions import LaunchConfiguration, PythonExpression
from launch.conditions import IfCondition
from launch_ros.actions import Node

def generate_launch_description():
    camera_arg = DeclareLaunchArgument('camera', default_value='d455', description='Type of camera to launch (d455 or l515)')

    parameters=[{
          'frame_id':'camera_link', # What frame it should track odom from (the loop closure graphics)
          'subscribe_scan_cloud':False,
          'subscribe_depth':True,
          'subscribe_rgbd':False,
          'subscribe_odom_info':False, 
          'approx_sync':True,
          'queue_size':10, 
          'Kp/MaxFeatures': '-1', 
          'RGBD/LinearUpdate': '0', 
          'Icp/VoxelSize' : '0.05',
          'Icp/MaxCorrespondenceDistance' : '0.1',
          'wait_for_transform':1.0}]

    remappings=[
          ('scan_cloud', '/camera/depth/color/points'),
          ('depth/image', '/camera/aligned_depth_to_color/image_raw'),
          ('rgb/image', '/camera/color/image_raw'),
          ('rgb/camera_info', '/camera/color/camera_info')]
    
    """
    # RealSense node configuration
    d455_launcher = Node(
        condition=IfCondition(PythonExpression([
                    "'", LaunchConfiguration('camera'), "'", 
                    " == 'd455'" 
                    ])),
        package='realsense2_camera',
        executable='realsense2_camera_node',
        name='realsense2_camera',
        output='screen',
        parameters=[{
                    'align_depth.enable': True,
                    'pointcloud.enable': True
                    }],
    )

    l515_launcher = Node(
        condition=IfCondition(PythonExpression([
                    "'", LaunchConfiguration('camera'), "'", 
                    " == 'l515'" 
                    ])),
        package='realsense2_camera',
        executable='realsense2_camera_node',
        name='realsense2_camera',
        output='screen',
        parameters=[{
                    'align_depth.enable': True, #var False når den funket sist
                    'pointcloud.enable': True,
                    'rgb_camera.profile': '640,480,30',
                    'depth_module.profile': '640,480,30'
                    }],
    )
    """
    

 
    # Odometry node configuration
    odometry_node = Node(
        condition=IfCondition(PythonExpression([
                    "'", LaunchConfiguration('camera'), "'", 
                    " == 'l515'",
                    " or "
                    "'", LaunchConfiguration('camera'), "'", 
                    " == 'd455'",
                    ])),
        package='robot_control',
        executable='odom.py',
        name='odometry',
        output='screen'
    )


    return LaunchDescription([
        camera_arg,
        #d455_launcher,
        #l515_launcher,
        odometry_node,
        Node(
            package='rtabmap_slam', executable='rtabmap', output='screen',
            parameters=parameters,
            remappings=remappings,
            arguments=['-d']),

        Node(
            package='rtabmap_viz', executable='rtabmap_viz', output='screen',
            parameters=parameters,
            remappings=remappings),
    ])