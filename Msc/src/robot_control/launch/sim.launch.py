from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument, ExecuteProcess, TimerAction, GroupAction
from launch.conditions import IfCondition, UnlessCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir, LaunchConfiguration, Command, FindExecutable, PathJoinSubstitution, PythonExpression
from launch_ros.substitutions import FindPackageShare
from launch_ros.actions import Node


def generate_launch_description():
    # Arguments
    ur_type_arg = DeclareLaunchArgument('ur_type', default_value='ur5e')
    robot_ip_arg = DeclareLaunchArgument('robot_ip', default_value='192.168.56.101')
    launch_rviz_arg = DeclareLaunchArgument('launch_rviz', default_value='true')
    camera_arg = DeclareLaunchArgument('camera', default_value='d455', description='Type of camera to launch (d455 or l515)')
    rtab_arg = DeclareLaunchArgument('rtab', default_value='true', description='To launch rtabmap')


    # Execute the start_ursim.sh script from the ur_robot_driver package
    start_ursim_cmd = ExecuteProcess(
        cmd=[
            PathJoinSubstitution([FindPackageShare('ur_robot_driver'), 'scripts', 'start_ursim.sh']),
            '-m', LaunchConfiguration('ur_type')
        ],
        shell=True
    )

    # Include the UR control launch file from the ur_robot_driver package, delayed by a TimerAction
    
    ur_control_launch = TimerAction(
        period=20.0,  # Delay in seconds
        actions=[IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('ur_robot_driver'),
                    'launch',
                    'ur_control.launch.py'
                ])
            ]),
            launch_arguments={
                'ur_type': LaunchConfiguration('ur_type'),
                'robot_ip': LaunchConfiguration('robot_ip'),
                'launch_rviz': LaunchConfiguration('launch_rviz')
            }.items(),
        )]
    )
    '''

    ur_control_launch = TimerAction(
        period=20.0,  # Delay in seconds
        actions=[IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('deepcobot_ur5e'),
                    'launch',
                    'robot.launch.py'
                ])
            ]),
            launch_arguments={
                'ur_type': LaunchConfiguration('ur_type'),
                'robot_ip': LaunchConfiguration('robot_ip'),
                'launch_rviz': LaunchConfiguration('launch_rviz')
            }.items(),
        )]
    )
    '''


    # Include the UR MoveIt launch file from the ur_moveit_config package, delayed by a TimerAction
    ur_moveit_launch = TimerAction(
        period=30.0,  # Delay in seconds
        actions=[IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('ur_moveit_config'),
                    'launch',
                    'ur_moveit.launch.py'
                ])
            ]),
            launch_arguments={'ur_type': LaunchConfiguration('ur_type')}.items(),
        )]
    )

    # Conditional and delayed Static TF publisher between tool0 and camera_frame for d455
    tf_d455 = TimerAction(
        period=15.0,  # Delay in seconds before executing the TF publisher
        actions=[GroupAction(
            actions=[
                Node(
                    condition=IfCondition(PythonExpression([
                    "'", LaunchConfiguration('camera'), "'", 
                    " == 'd455'" 
                    ])),
                    package='tf2_ros',
                    executable='static_transform_publisher',
                    name='static_tf_pub_tool0_to_camera',
                    arguments=['0.04449861', '0.0', '0.04687861', '0.0', '0.0', '0.7071068', '0.7071068', 'tool0', 'camera_link']  # Parameters for D455
                )
            ]
        )]
    )

    tf_l515  = TimerAction(
        period=15.0,  # Delay in seconds before executing the TF publisher
        actions=[GroupAction(
            actions=[
                Node(
                    condition=IfCondition(PythonExpression([
                    "'", LaunchConfiguration('camera'), "'", 
                    " == 'l515'" 
                    ])),
                    package='tf2_ros',
                    executable='static_transform_publisher',
                    name='static_tf_pub_tool0_to_camera',
                    arguments=['0.0', '-0.0575', '0.0', '0.0', '-0.7071068', '0.0', '0.7071068', 'tool0', 'camera_link']  # Parameters for L515
                )
            ]
        )]
    )
    
    # Conditional and delayed launch for d455 camera
    rtab_launcher = TimerAction(
        period=17.0,  # Delay in seconds before launching d455 camera
        actions=[IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('robot_control'),  # Replace with your actual package name
                    'launch',
                    'rtab.launch.py'
                ])
            ]),
            launch_arguments={'camera': LaunchConfiguration('camera')}.items(),
            condition=IfCondition(PythonExpression([
                    "'", LaunchConfiguration('rtab'), "'", 
                    " == 'true'" 
                    ])),
        )]
    )

    return LaunchDescription([
        
        ur_type_arg,
        robot_ip_arg,
        launch_rviz_arg,
        camera_arg,
        rtab_arg,
        start_ursim_cmd,
        ur_control_launch,
        ur_moveit_launch,
        Node(
            package='tf2_ros',
            executable='static_transform_publisher',
            name='static_transform_publisher',
            output='screen',
            arguments=['0.0', '0.0', '0.0', '0.0', '0.0', '0.0', 'base_link', 'touch_link']
        ),
        #tf_d455,
        #tf_l515,
        #rtab_launcher,
       
    ])
