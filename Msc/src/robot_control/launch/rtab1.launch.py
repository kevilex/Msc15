# Requirements:
#   A realsense D400 series
#   Install realsense2 ros2 package (make sure you have this patch: https://github.com/IntelRealSense/realsense-ros/issues/2564#issuecomment-1336288238)
# Example:
#   $ ros2 launch realsense2_camera rs_launch.py align_depth.enable:=true
#   OR
#   $ ros2 launch realsense2_camera rs_launch.py align_depth.enable:=false pointcloud.enable:=true rgb_camera.profile:='640,480,30' depth_module.profile:='640,480,30'
#
#   $ ros2 launch rtabmap_examples realsense_d400.launch.py
#   OR
#   $ ros2 launch rtabmap_launch rtabmap.launch.py frame_id:=camera_link args:="-d" rgb_topic:=/camera/color/image_raw depth_topic:=/camera/aligned_depth_to_color/image_raw camera_info_topic:=/camera/color/camera_info approx_sync:=false
# $
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, SetEnvironmentVariable
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node

def generate_launch_description():
    parameters=[{
          'frame_id':'camera_link', #stod base_link her før, men funket bra med camera_link
          #'subscribe_scan_cloud':True,
          'subscribe_depth':True, #swap this to False if own camera node
          'subscribe_odom_info':False,
          'approx_sync':True,
          'queue_size':10,
          'Kp/MaxFeatures': '-1',
          'RGBD/LinearUpdate': '0',
          'wait_for_transform':0.3}]

    remappings=[
          #('rgb/image', '/color/image_raw'),
          #('rgb/camera_info', '/color/camera_info'),
          #('scan_cloud', '/camera/depth_stream'),
          ('rgb/image', '/camera/color/image_raw'),
          ('rgb/camera_info', '/camera/color/camera_info'),
          ('depth/image', '/camera/depth/image_rect_raw')]

    return LaunchDescription([
        # Nodes to launch
        Node(
            package='rtabmap_slam', executable='rtabmap', output='screen',
            parameters=parameters,
            remappings=remappings,
            arguments=['-d']),

        Node(
            package='rtabmap_viz', executable='rtabmap_viz', output='screen',
            parameters=parameters,
            remappings=remappings),
    ])