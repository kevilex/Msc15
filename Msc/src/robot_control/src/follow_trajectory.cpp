#include <memory>
#include <rclcpp/rclcpp.hpp>
#include <moveit/move_group_interface/move_group_interface.h>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <Eigen/Geometry> 
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <moveit_msgs/msg/collision_object.hpp>
#include <shape_msgs/msg/solid_primitive.hpp>

struct Point {
    double x, y, z;
};


std::vector<Point> parsePLYFile(const std::string& filename) {
    std::ifstream file(filename);
    std::vector<Point> points;
    std::string line;

    if (!file.is_open()) {
        throw std::runtime_error("Unable to open file");
    }

    // Read vertex data
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        Point p;
        if (!(iss >> p.x >> p.y >> p.z)) { 
            break; // Error or end of file
        }
        points.push_back(p);
    }

    return points;
}

Eigen::Quaterniond vectorToQuaternion(const Point& target_vector) {
    // Assuming the initial direction is along the z-axis
    Eigen::Vector3d initial_direction(0, 0, 1);
    Eigen::Vector3d target_direction(target_vector.x, target_vector.y, target_vector.z);

    // Normalize the target direction
    target_direction.normalize();

    // Compute the rotation axis and angle
    Eigen::Vector3d rotation_axis = initial_direction.cross(target_direction);
    double angle = acos(initial_direction.dot(target_direction));

    // Create and return the quaternion representing this rotation
    Eigen::Quaterniond quaternion;
    quaternion = Eigen::AngleAxisd(angle, rotation_axis.normalized());

    return quaternion;
}


std::vector<Eigen::Quaterniond> pointsToQuaternions(const std::vector<Point>& points) {
    std::vector<Eigen::Quaterniond> quaternions;

    for (const Point& point : points) {
        Eigen::Quaterniond quaternion = vectorToQuaternion(point);
        quaternions.push_back(quaternion);
    }

    return quaternions;
}

int main(int argc, char* argv[])
{

    // Initialize ROS and create the Node
    rclcpp::init(argc, argv);
    auto const node = std::make_shared<rclcpp::Node>(
        "Manipulator_Node", rclcpp::NodeOptions().automatically_declare_parameters_from_overrides(true));

    // Create a ROS logger
    auto const logger = rclcpp::get_logger("Manipulator_Node");

    // Create the MoveIt MoveGroup Interface
    using moveit::planning_interface::MoveGroupInterface;
    auto move_group_interface = MoveGroupInterface(node, "ur_manipulator");

    // Create the MoveIt PlanningSceneInterface
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

    // Adding the table
    moveit_msgs::msg::CollisionObject collision_object;
    collision_object.header.frame_id = move_group_interface.getPlanningFrame();
    collision_object.id = "Table";

    // Define the pose of the table
    geometry_msgs::msg::Pose table_pose;
    table_pose.orientation.z = 0.0;
    table_pose.orientation.w = 0.0;
    table_pose.position.x = -0.0;
    table_pose.position.z = -0.35 - 0.005;

    // Define the shape and dimensions of the table (box)
    shape_msgs::msg::SolidPrimitive primitive;
    primitive.type = primitive.BOX;
    primitive.dimensions.resize(3);
    primitive.dimensions[primitive.BOX_X] = 0.3;
    primitive.dimensions[primitive.BOX_Y] = 0.3;
    primitive.dimensions[primitive.BOX_Z] = 0.7;

    collision_object.primitives.push_back(primitive);
    collision_object.primitive_poses.push_back(table_pose);
    collision_object.operation = collision_object.ADD;
    
    // Add the collision object into the world
    std::vector<moveit_msgs::msg::CollisionObject> collision_objects;
    collision_objects.push_back(collision_object);
    planning_scene_interface.addCollisionObjects(collision_objects);


    // Set a lower velocity scaling factor for smoother motions
    const double velocity_scaling_factor = 0.0001; // Adjust this value as needed
    move_group_interface.setMaxVelocityScalingFactor(velocity_scaling_factor);
    move_group_interface.setMaxAccelerationScalingFactor(velocity_scaling_factor);


    // Define multiple target poses and add them to a vector
    std::vector<geometry_msgs::msg::Pose> waypoints;

    // Retrieve waypoints
    std::string Path = "/home/kevin/workspacegl/Msc15/control/points_output.txt";
    std::vector<Point> points = parsePLYFile(Path);

    std::string Orientation = "/home/kevin/workspacegl/Msc15/control/orientation_output.txt";
    std::vector<Point> angle_vectors = parsePLYFile(Orientation);


    // Calculate the normal vector for the waypoints
    std::vector<Eigen::Quaterniond> angle_quaternion = pointsToQuaternions(angle_vectors);

    // Adding all the points to the waypoints vector
    int num_points = static_cast<int>(points.size());
    RCLCPP_INFO(node->get_logger(), "Adding %i points:", num_points);
    for (int i = 0; i < num_points; ++i) {

        geometry_msgs::msg::Pose temp;
        temp.orientation.x = angle_quaternion[i].x();
        temp.orientation.y = angle_quaternion[i].y();
        temp.orientation.z = angle_quaternion[i].z();
        temp.orientation.w = angle_quaternion[i].w();

        temp.position.x = points[i].x;
        temp.position.y = points[i].y;
        temp.position.z = points[i].z;
        waypoints.push_back(temp);

        RCLCPP_INFO(node->get_logger(), "x: %f, y: %f, z: %f", temp.position.x, temp.position.y, temp.position.z);
    }

    if(waypoints.size() == 0){
        RCLCPP_INFO(node->get_logger(), "Zero plan detected");
    }



    // Plan the Cartesian path connecting the waypoints
    moveit_msgs::msg::RobotTrajectory trajectory;
    const double jump_threshold = 10.00; // Threshold for deciding if a movement is a 'jump'
    const double eef_step = 0.01; // Resolution of the path
    
    double fraction = move_group_interface.computeCartesianPath(waypoints, eef_step, jump_threshold, trajectory);
    


    if (fraction > 0.01)
    {
        // Execute the plan
        move_group_interface.execute(trajectory);
       
    }
    else
    {
        RCLCPP_ERROR(logger, "Planning failed!");
    }

    // Shutdown ROS
    rclcpp::shutdown();
    return 0;
}