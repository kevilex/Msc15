import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PoseStamped, PoseArray, Pose, Point, Quaternion
from builtin_interfaces.msg import Duration
import math, numpy as np

def vector_to_quaternion(x, y, z):

    v1 = np.array((1,0,0))
    v2 = np.array((x,y,z))

    cross_product = np.cross(v1,v2)
                  
    dot_product = np.dot(v1, v2)
    magnitude_product = np.linalg.norm(v1) * np.linalg.norm(v2)
    angle = np.arccos(dot_product / magnitude_product)

    

    # Calculate the quaternion components
    qw = math.cos(angle / 2)
    qx = cross_product[0] * math.sin(angle / 2)
    qy = cross_product[1] * math.sin(angle / 2)
    qz = cross_product[2] * math.sin(angle / 2)

    return Quaternion(x=qx, y=qy, z=qz, w=qw)

class PosePublisher(Node):
    def __init__(self):
        super().__init__('pose_publisher')
        self.publisher_ = self.create_publisher(PoseArray, '/pose_array/debug', 10)
        self.timer_period = 10  # in seconds
        self.timer = self.create_timer(self.timer_period, self.timer_callback)
        self.orientations = []
        self.positions = []

        with open("orientation_output.txt", "r") as file:
            for line in file:
                rx, ry, rz = map(float, line.split())
                quaternion = vector_to_quaternion((rx), (ry), (rz))
                self.orientations.append(quaternion)

        with open("points_output.txt", "r") as file:
            for line in file:
                x, y, z = map(float, line.split())
                self.positions.append(Point(x=x, y=y, z=z))

    def timer_callback(self):
        pose_array = PoseArray()
        pose_array.header.frame_id = "world"
        pose_array.header.stamp = self.get_clock().now().to_msg()

        for i in range(len(self.orientations)):
            pose = Pose()
            pose.position = self.positions[i]
            pose.orientation = self.orientations[i]
            pose_array.poses.append(pose)

        self.publisher_.publish(pose_array)
        self.get_logger().info('Published PoseArray with %d poses' % len(pose_array.poses))

def main(args=None):
    rclpy.init(args=args)
    pose_publisher = PosePublisher()
    rclpy.spin(pose_publisher)
    pose_publisher.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
