#!/usr/bin/env python3
import rclpy, lib

def main(args=None):
    rclpy.init(args=args)
    serverhandler = lib.serverhandler('http://127.0.0.1:5000/upload',
                                      'http://127.0.0.1:5000/download/')
    camera = lib.camera()
    joy_to_robot = lib.JoyToRobot(camera, serverhandler)

    rclpy.spin(joy_to_robot)
    joy_to_robot.destroy()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
