import rclpy, tf2_ros
from rclpy.time import Time
from rclpy.qos import QoSProfile
from rclpy.node import Node
from sensor_msgs.msg import Joy, PointCloud2, PointField, Image, CameraInfo
from geometry_msgs.msg import TwistStamped, TransformStamped
from control_msgs.msg import JointJog
from std_srvs.srv import Trigger
from moveit_msgs.msg import PlanningScene, CollisionObject
import pyrealsense2 as rs
import threading, ros2_numpy, numpy as np, array, time, tf2_py as tf2, vedo
from cv_bridge import CvBridge
from tf2_ros import LookupException, ConnectivityException, ExtrapolationException
#from tf2_ros import quaternion, translation
#from tf.transformations.quaternion_matrix

class JoyToServoPub(Node):
    def __init__(self):
        super().__init__('joy_to_twist_publisher')
        self.frame_to_publish = 'tool0'

        # Setup pub/sub control
        self.joy_sub = self.create_subscription(
            Joy, '/joy', self.joy_callback, 15)
        self.twist_pub = self.create_publisher(
            TwistStamped, '/servo_node/delta_twist_cmds', 15)
        self.joint_pub = self.create_publisher(
            JointJog, '/servo_node/delta_joint_cmds', 15)
        self.collision_pub = self.create_publisher(
            PlanningScene, '/planning_scene', 10)
        # Setup pubs for cameras
        self.pc_pub = self.create_publisher(
            PointCloud2, 'camera/depth', 10)
        self.color_pub = self.create_publisher(
            Image, 'color/image_raw', 10)
        self.camera_info_publisher = self.create_publisher(
            CameraInfo, 'color/camera_info', 10)

        
        #tf 
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, self)
        
        #camera
        self.pipe = rs.pipeline()
        self.cfg = rs.config()
        self.cfg.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        self.cfg.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
        self.pc = rs.pointcloud()
        self.profile = self.pipe.start(self.cfg)
        self.intrinsics = self.profile.get_stream(rs.stream.color).as_video_stream_profile().get_intrinsics()
        self.temporal = rs.temporal_filter()
        self.decimation = rs.decimation_filter(magnitude = 5)

        #for alignment
        align_to = rs.stream.depth
        self.align = rs.align(align_to)
        self.msg = PointCloud2()
        self.points = np.array([], dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])
        self.filtered_points = np.array([], dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])
        self.msgCount = 1

        #bridge for posting camera information easily
        self.bridge = CvBridge()

        #camera info msg
        self.camera_info_msg = CameraInfo()
        self.camera_info_msg.width = self.intrinsics.width
        self.camera_info_msg.height = self.intrinsics.height
        self.camera_info_msg.distortion_model = 'plumb_bob'
        self.camera_info_msg.d = [0.0, 0.0, 0.0, 0.0, 0.0]  # Assuming no distortion
        self.camera_info_msg.k = [self.intrinsics.fx, 0.0, self.intrinsics.ppx,
                             0.0, self.intrinsics.fy, self.intrinsics.ppy,
                             0.0, 0.0, 1.0]
        self.camera_info_msg.r = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        self.camera_info_msg.p = [self.intrinsics.fx, 0.0, self.intrinsics.ppx, 0.0,
                             0.0, self.intrinsics.fy, self.intrinsics.ppy, 0.0,
                             0.0, 0.0, 1.0, 0.0]
        
        #PC filtering
        self.filterradius = 0.001

        # Load the collision scene asynchronously
        self.collision_pub_thread = threading.Thread(target=self.load_collision_scene)
        self.collision_pub_thread.start()
        self.AXIS_DEFAULTS = {5: 1.0, 2: 1.0}

        self.lastcommand = time.time()
        print('Playstation mode initiated')
        print('--------------------------------')
        print('Cross: Take picture and betwixtnt saving')
        print('Circle: Save combined pointcloud')
        print('Triangle: Reset Scan')


    def joy_callback(self, msg):
        # Create the messages we publish
        twist_msg = TwistStamped()
        joint_msg = JointJog()

        # This call updates the frame for twist commands
        self.update_cmd_frame(msg.axes)
        # Convert the joystick message to Twist or JointJog and publish
        if self.convert_joy_to_cmd(msg.axes, msg.buttons, twist_msg, joint_msg):
            # publish the TwistStamped
            twist_msg.header.frame_id = self.frame_to_publish
            twist_msg.header.stamp = self.get_clock().now().to_msg()
            self.twist_pub.publish(twist_msg)
            # publish the JointStamped
            joint_msg.header.stamp = self.get_clock().now().to_msg()
            joint_msg.header.frame_id = 'base_link'
            self.joint_pub.publish(joint_msg)

        self.camera_color()

    def convert_joy_to_cmd(self, axes, buttons, twist, joint):
        # Camera functionality to the buttons
        if buttons[0]:
            if time.time() - self.lastcommand > 2:
                self.publish_pc()
                self.lastcommand = time.time()
                print('picture taken, saved as intermittent_points' + str(self.msgCount) + '.ply')
        if buttons[1]:
            if time.time() - self.lastcommand > 2:
                self.save_points_as_ply(self.points, 'collected/combined.ply')
                self.lastcommand = time.time()
                print('combined picture saved as combined.ply')

        if buttons[2]:
            if time.time() - self.lastcommand > 2:
                self.points = np.array([], dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])
                self.msgCount = 1
                self.lastcommand = time.time()
                print('reset points')


        
        if buttons[8]: 
            joint.joint_names = ['shoulder_pan_joint']
            joint.velocities = [-5.0]
        elif buttons[9]:  
            joint.joint_names = ['shoulder_pan_joint']
            joint.velocities = [5.0] 
        else:
            joint.joint_names = ['shoulder_pan_joint']
            joint.velocities = []

        # Mapping buttons and axes to twist commands
        twist.twist.linear.z = axes[4]
        twist.twist.linear.y = axes[3]

        lin_x_right = -0.5 * (axes[5] - self.AXIS_DEFAULTS.get(5, 1.0))
        lin_x_left = 0.5 * (axes[2] - self.AXIS_DEFAULTS.get(2, 1.0))
        twist.twist.linear.x = lin_x_right + lin_x_left

        twist.twist.angular.y = axes[1]
        twist.twist.angular.x = axes[0]

        roll_positive = buttons[5]
        roll_negative = -1 * buttons[4]
        twist.twist.angular.y = float(roll_positive + roll_negative)
        return True
        
    def update_cmd_frame(self, axes):
        if (axes[7] < -0.1) and self.frame_to_publish == 'tool0':
            self.frame_to_publish = 'base_link'
        elif (axes[7] > 0.1) and self.frame_to_publish == 'base_link':
            self.frame_to_publish = 'tool0'

    def load_collision_scene(self):
        time.sleep(3)
        # Create a PlanningScene message
        planning_scene = PlanningScene()
        planning_scene.is_diff = True

        # Create collision object, in the way of servoing
        collision_object = CollisionObject()
        collision_object.id = 'box'

        # ... (same collision scene setup as in C++ code)

        planning_scene.world.collision_objects.append(collision_object)

        self.collision_pub.publish(planning_scene)

    def camera_pc(self):
        frames = []
        # Grab ten frames
        for x in range(10):
            frameset = self.pipe.wait_for_frames()
            db = frameset.get_depth_frame()
            frames.append(db)

        
        # Use the temporal filter
        for x in range(10):
            temp_filtered = self.temporal.process(frames[x])
        
        # Use the decimation filter
        decimated_depth = self.decimation.process(temp_filtered)

        # Retrieve and return the points
        points = self.pc.calculate(decimated_depth)
        return points

    def publish_pc(self):

        # Retrieve filtered pointcloud
        points = self.camera_pc()
        points = np.asanyarray(points.get_vertices(), dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])


        # Retrieve transform data
        transform = self.get_transform('camera_link', 'base_link') #camera, base_link with robot tf | marker_camera -> marker_x with markers
        if transform is None:
            print('No transform')
            return  # or handle the error as appropriate
        rot_matrix, trans_matrix = self.quaternion_to_matrix(transform)
        transformed_points_list = []

        # Apply the transformation
        for point in points:
            transformed_point = self.transform_point(point, rot_matrix, trans_matrix)
            transformed_points_list.append((transformed_point['f0'], transformed_point['f1'], transformed_point['f2']))

        # Transformed array and save to ply
        transformed_points_array = np.array(transformed_points_list, dtype=self.points.dtype)        
        self.save_points_as_ply(transformed_points_array, ('collected/intermittent_points' + str(self.msgCount) + '.ply'))

        # Add transformed points to the total points array
        self.points = np.concatenate((self.points, transformed_points_array))

        #create the pointcloud message        
        self.msg.header.frame_id = 'base_link' #base_link with robot, if not marker200
        current_time = time.time()
        self.msg.header.stamp.sec = int(current_time)
        self.msg.header.stamp.nanosec = int((current_time - self.msg.header.stamp.sec) * 1e9)
        self.msg.height = 1
        self.msg.width += len(points)
        self.msg.point_step = 12
        self.msg.row_step = self.msg.point_step * self.msg.width
        self.msg.fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1)]
        self.msg.is_bigendian = False
        self.msg.is_dense = False

        # Flatten the data
        flattened = np.hstack((self.points['f0'].reshape(-1, 1), self.points['f1'].reshape(-1, 1), self.points['f2'].reshape(-1, 1)))
        self.msgCount = self.msgCount + 1
        
        # Add the flattened data to the message
        self.msg.data = np.asarray(flattened, np.float32).tobytes()

        self.pc_pub.publish(self.msg)

    def camera_color(self):
        # Retrieve frame
        frame = self.pipe.wait_for_frames()
        color_frame = frame.get_color_frame()
        if not color_frame:
            return
        #If frame, convert to imgmsg and publish color frame
        color_image = np.asanyarray(color_frame.get_data())
        ros_image = self.bridge.cv2_to_imgmsg(color_image, "bgr8")
        self.color_pub.publish(ros_image)
        # Post camera info
        self.camera_info_msg.header = ros_image.header 
        self.camera_info_publisher.publish(self.camera_info_msg)


    def save_points_as_ply(self, points, filename):
        with open(filename, 'w') as ply_file:
            # Write PLY header
            ply_file.write("ply\n")
            ply_file.write("format ascii 1.0\n")
            ply_file.write(f"element vertex {len(points)}\n")
            ply_file.write("property float x\n")
            ply_file.write("property float y\n")
            ply_file.write("property float z\n")
            ply_file.write("end_header\n")

            # Write point data
            for point in points:
                ply_file.write(f"{point['f0']} {point['f1']} {point['f2']}\n")


    def quaternion_to_matrix(self, transform):
        x, y, z, w = transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w
        xx, yy, zz, xy, xz, yz, wx, wy, wz = (x * x, y * y, z * z, x * y, x * z, y * z, w * x, w * y, w * z)

        rot_matrix = np.array([
            [1 - 2 * (yy + zz),     2 * (xy - wz),     2 * (xz + wy), 0],
            [    2 * (xy + wz), 1 - 2 * (xx + zz),     2 * (yz - wx), 0],
            [    2 * (xz - wy),     2 * (yz + wx), 1 - 2 * (xx + yy), 0],
            [                0,                 0,                 0, 1]
        ])


        # Add the translation part
        trans_matrix = [transform.translation.x, transform.translation.y, transform.translation.z]
        return rot_matrix, trans_matrix
    

    def transform_point(self, point, rot_matrix, trans_matrix):
        # Extracting xyz from point
        pointt = np.array([point['f0'], point['f1'], point['f2'] , 1])
        # Dot product the rotational matrix with the point
        rotated_point = np.dot(rot_matrix, pointt)
        # Add the translation vector to the rotated point and return transformed point
        transformed_point = np.array([rotated_point[0] + trans_matrix[0], rotated_point[1] + trans_matrix[1], rotated_point[2] + trans_matrix[2], 1])
        return {'f0': transformed_point[0], 'f1': transformed_point[1], 'f2': transformed_point[2]}


    def get_transform(self, from_frame, to_frame):
        try:
            now = rclpy.time.Time()
            trans = self.tf_buffer.lookup_transform(to_frame, from_frame, now)
            return trans.transform
        except (LookupException, ConnectivityException, ExtrapolationException) as ex:
            self.get_logger().error('Could not transform %s to %s: %s' % (from_frame, to_frame, ex))
            return None
        
    def pc_filtration(self, newscan, oldscan):
        filtered_points = []
        streamed_points = []
        p1_f0, p1_f1, p1_f2 = newscan['f0'], newscan['f1'], newscan['f2']

        zeropoint = newscan[0]
        zeropoint['f0'] = 0
        zeropoint['f1'] = 0
        zeropoint['f2'] = 0
    
        for point1 in newscan:
            diffs = [(oldscan['f0'].reshape(-1, 1) - p1_f0), 
                (oldscan['f1'].reshape(-1, 1) - p1_f1),  
                (oldscan['f2'].reshape(-1, 1) - p1_f2)]

            # Check if the minimum squared distance is greater than the squared radius (no points within the radius)
            if np.any(diffs > self.filterradius):
                # This point in points1 is not within the radius of any point in points2, so it's not a duplicate
                streamed_points.append(point1)
                filtered_points.append(point1)
            else:
                streamed_points.append(zeropoint)

        filtered_points = np.array(filtered_points, dtype=oldscan.dtype)
        streamed_points = np.array(filtered_points, dtype=oldscan.dtype)
        return streamed_points, filtered_points

               
    def destroy(self):
        if self.collision_pub_thread.is_alive():
            self.collision_pub_thread.join()
    
    def __del__(self):
        self.pipe.stop()



def main(args=None):
    rclpy.init(args=args)
    joy_to_servo_pub = JoyToServoPub()
    rclpy.spin(joy_to_servo_pub)
    joy_to_servo_pub.destroy()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
