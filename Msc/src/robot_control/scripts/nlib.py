import rclpy, tf2_ros, threading, subprocess, pyvista as pv, pyrealsense2 as rs, threading, time
from rclpy.time import Time
from rclpy.qos import QoSProfile
from rclpy.node import Node
from sensor_msgs.msg import Joy, PointCloud2, PointField, Image, CameraInfo
from geometry_msgs.msg import TwistStamped
from moveit_msgs.msg import PlanningScene, CollisionObject
import numpy as np, time, tf2_py as tf2, requests
from cv_bridge import CvBridge
from tf2_ros import LookupException, ConnectivityException, ExtrapolationException
from controller_manager_msgs.srv import SwitchController, LoadController
from std_srvs.srv import Trigger
from nav_msgs.msg import Odometry 


class camera():
    def __init__(self):
        '''
        # Realsense setup
        self.pipe = rs.pipeline()
        self.cfg = rs.config()
        self.cfg.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        self.cfg.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
        self.pc = rs.pointcloud()
        self.profile = self.pipe.start(self.cfg)
        self.intrinsics = self.profile.get_stream(rs.stream.color).as_video_stream_profile().get_intrinsics()
        self.temporal = rs.temporal_filter()
        self.decimation = rs.decimation_filter(magnitude = 5)

        # Pointcloud datatype setup
        self.msg = PointCloud2()
        self.points = np.array([], dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])
        self.pointsstream = np.array([], dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])
        self.msgCount = 1
        self.msgStreamCount = 1

        # Bridge for easy converting of color frames
        self.bridge = CvBridge()

        align_to = rs.stream.color
        self.align = rs.align(align_to)


        # Camera info message
        self.camera_info_msg = CameraInfo()
        self.camera_info_msg.width = self.intrinsics.width
        self.camera_info_msg.height = self.intrinsics.height
        self.camera_info_msg.distortion_model = 'plumb_bob'
        self.camera_info_msg.d = [0.0, 0.0, 0.0, 0.0, 0.0]  # Assuming no distortion
        self.camera_info_msg.k = [self.intrinsics.fx, 0.0, self.intrinsics.ppx,
                                0.0, self.intrinsics.fy, self.intrinsics.ppy,
                                0.0, 0.0, 1.0]
        self.camera_info_msg.r = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        self.camera_info_msg.p = [self.intrinsics.fx, 0.0, self.intrinsics.ppx, 0.0,
                                0.0, self.intrinsics.fy, self.intrinsics.ppy, 0.0,
                                0.0, 0.0, 1.0, 0.0]

        self.frames = []
        
        # Grab ten frames
        for x in range(8):
            frameset = self.pipe.wait_for_frames()
            aligned_frames = self.align.process(frameset)
            
            aligned_depth_frame = aligned_frames.get_depth_frame()
            self.frames.append(aligned_depth_frame)

        # Use the temporal filter
        for x in range(8):
            temp_filtered = self.temporal.process(self.frames[x])
        
        # Use the decimation filter
        decimated_depth = self.decimation.process(temp_filtered)

        # Retrieve and return the points
        self.rawpoints = self.pc.calculate(decimated_depth)
    
        picthread = threading.Thread(target=self._continous_frames)
        picthread.start()

        self.color_image = None


        self.lastpic = time.time()
        '''
        
    def camera_frame(self, imageType):
        if imageType == 'pointcloud':
            frames = []
            # Grab ten frames
            for x in range(8):
                frameset = self.pipe.wait_for_frames()
                aligned_frames = self.align.process(frameset)
                
                aligned_depth_frame = aligned_frames.get_depth_frame()
                frames.append(aligned_depth_frame)

            
            # Use the temporal filter
            for x in range(10):
                temp_filtered = self.temporal.process(frames[x])
            
            # Use the decimation filter
            decimated_depth = self.decimation.process(temp_filtered)

            # Retrieve and return the points
            points = self.pc.calculate(decimated_depth)
            return points
        elif(imageType == 'colorframe'):
            # Retrieve frame
            #print(time.time() - self.lastpic)
            #poll_for_frames
            frame = self.pipe.wait_for_frames()
            color_frame = frame.get_color_frame()
            if not color_frame:
                return
            color_image = np.asanyarray(color_frame.get_data())
            #self.lastpic = time.time()
            return color_image

    def save_points(self, publisher, camframe, origo, tf_buffer, save=False):
        # Retrieve filtered pointcloud
        points = self.rawpoints
        points = np.asanyarray(points.get_vertices(), dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])
        print(points)

        if camframe is not None:
            # Retrieve transform data
            transform = self._get_transform((camframe), (origo), tf_buffer) #camera, base_link with robot tf | marker_camera -> marker_x with markers
            if transform is None:
                print('No transform')
                return  # or handle the error as appropriate
            rot_matrix, trans_matrix = self._quaternion_to_matrix(transform)
            transformed_points_list = []

            # Apply the transformation
            for point in points:
                transformed_point = self._transform_point(point, rot_matrix, trans_matrix)
                transformed_points_list.append((transformed_point['f0'], transformed_point['f1'], transformed_point['f2']))

            points = np.array(transformed_points_list, dtype=self.points.dtype)     
            print(points) 

        if save:
            # Transformed array and save to ply
            self.save_points_as_ply(('collected/intermittent_points' + str(self.msgCount) + '.ply'), points)

        # Add transformed points to the total points array
        self.points = np.concatenate((self.points, points))

        #create the pointcloud message        
        self.msg.header.frame_id = str(origo) #base_link with robot, if not marker200
        current_time = time.time()
        self.msg.header.stamp.sec = int(current_time)
        self.msg.header.stamp.nanosec = int((current_time - self.msg.header.stamp.sec) * 1e9)
        self.msg.height = 1
        self.msg.width = len(points) * self.msgCount
        self.msg.point_step = 12
        self.msg.row_step = self.msg.point_step * self.msg.width
        self.msg.fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1)]
        self.msg.is_bigendian = False
        self.msg.is_dense = False

        # Flatten the data
        flattened = np.hstack((self.points['f0'].reshape(-1, 1), self.points['f1'].reshape(-1, 1), self.points['f2'].reshape(-1, 1)))
        self.msgCount = self.msgCount + 1
        
        # Add the flattened data to the message
        self.msg.data = np.asarray(flattened, np.float32).tobytes()

        publisher.publish(self.msg)
        return ('collected/intermittent_points' + str(self.msgCount) + '.ply')

    def publish_pc_stream(self, publisher, origo, camframe, tf_buffer, stamp):
        # Retrieve filtered pointcloud
        points = self.rawpoints
        points = np.asanyarray(points.get_vertices(), dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])

        if camframe is not None:
            # Retrieve transform data
            try:
                transform = self._get_transform((camframe), (origo), tf_buffer) #camera, base_link with robot tf | marker_camera -> marker_x with markers
                if transform is None:
                    print('No transform')
                    return  # or handle the error as appropriate
                
            except Exception as e:
                print(e)


            rot_matrix, trans_matrix = self._quaternion_to_matrix(transform)
            transformed_points_list = []

            # Apply the transformation
            for point in points:
                transformed_point = self._transform_point(point, rot_matrix, trans_matrix)
                transformed_points_list.append((transformed_point['f0'], transformed_point['f1'], transformed_point['f2']))

            points = np.array(transformed_points_list, dtype=self.pointsstream.dtype)      

        #create the pointcloud message        
        self.msg.header.frame_id = str(origo) #base_link with robot, if not marker200
        self.msg.header.stamp = stamp
        self.msg.height = 1
        self.msg.width = len(points)
        self.msg.point_step = 12
        self.msg.row_step = self.msg.point_step * self.msg.width
        self.msg.fields = [
            PointField(name='x', offset=0, datatype=PointField.FLOAT32, count=1),
            PointField(name='y', offset=4, datatype=PointField.FLOAT32, count=1),
            PointField(name='z', offset=8, datatype=PointField.FLOAT32, count=1)]
        self.msg.is_bigendian = False
        self.msg.is_dense = False

        # Flatten the data
        flattened = np.hstack((points['f0'].reshape(-1, 1), points['f1'].reshape(-1, 1), points['f2'].reshape(-1, 1)))
        self.msgStreamCount = self.msgStreamCount + 1
        
        # Add the flattened data to the message
        self.msg.data = np.asarray(flattened, np.float32).tobytes()

        publisher.publish(self.msg)

    def camera_color_publish(self, framepub, infopub, stamp):
        if self.color_image is not None:
            # Retrieve frame
            color_image = self.color_image
            ros_image = self.bridge.cv2_to_imgmsg(color_image, "bgr8")
            ros_image.header.stamp = stamp
            framepub.publish(ros_image)
            # Post camera info
            self.camera_info_msg.header = ros_image.header 
            infopub.publish(self.camera_info_msg)

    def save_points_as_ply(self, filename, points):
        with open(filename, 'w') as ply_file:
            # Write PLY header
            ply_file.write("ply\n")
            ply_file.write("format ascii 1.0\n")
            ply_file.write(f"element vertex {len(points)}\n")
            ply_file.write("property float x\n")
            ply_file.write("property float y\n")
            ply_file.write("property float z\n")
            ply_file.write("end_header\n")

            # Write point data
            for point in points:
                ply_file.write(f"{point['f0']} {point['f1']} {point['f2']}\n")


    def reset_points(self):
        self.points = np.array([], dtype=[('f0', '<f4'), ('f1', '<f4'), ('f2', '<f4')])
        self.msgCount = 1

    def _continous_frames(self):
        while True:
            # Grab new frame
            frameset = self.pipe.wait_for_frames()
            aligned_frames = self.align.process(frameset)
            
            aligned_depth_frame = aligned_frames.get_depth_frame()
            self.frames.append(aligned_depth_frame)

            # Delete oldest frame
            self.frames.pop(0)

            # Use the temporal filter
            for x in range(8):
                temp_filtered = self.temporal.process(self.frames[x])
            
            # Use the decimation filter
            decimated_depth = self.decimation.process(temp_filtered)

            # Retrieve and return the points
            self.rawpoints = self.pc.calculate(decimated_depth)

            color_frame = frameset.get_color_frame()
            color_image = np.asanyarray(color_frame.get_data())

            self.color_image = color_image
        
    def _quaternion_to_matrix(self, transform):
        x, y, z, w = transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w
        xx, yy, zz, xy, xz, yz, wx, wy, wz = (x * x, y * y, z * z, x * y, x * z, y * z, w * x, w * y, w * z)

        rot_matrix = np.array([
            [1 - 2 * (yy + zz),     2 * (xy - wz),     2 * (xz + wy), 0],
            [    2 * (xy + wz), 1 - 2 * (xx + zz),     2 * (yz - wx), 0],
            [    2 * (xz - wy),     2 * (yz + wx), 1 - 2 * (xx + yy), 0],
            [                0,                 0,                 0, 1]
        ])


        # Add the translation part
        trans_matrix = [transform.translation.x, transform.translation.y, transform.translation.z]
        return rot_matrix, trans_matrix
    
    def _transform_point(self, point, rot_matrix, trans_matrix):
        # Extracting xyz from point
        pointt = np.array([point['f0'], point['f1'], point['f2'] , 1])
        # Dot product the rotational matrix with the point
        rotated_point = np.dot(rot_matrix, pointt)
        # Add the translation vector to the rotated point and return transformed point
        transformed_point = np.array([rotated_point[0] + trans_matrix[0], rotated_point[1] + trans_matrix[1], rotated_point[2] + trans_matrix[2], 1])
        return {'f0': transformed_point[0], 'f1': transformed_point[1], 'f2': transformed_point[2]}

    def _get_transform(self, from_frame, to_frame, tf_buffer):
        try:
            now = rclpy.time.Time()
            trans = tf_buffer.lookup_transform(to_frame, from_frame, now)
            return trans.transform
        except (LookupException, ConnectivityException, ExtrapolationException) as ex:
            #self.get_logger().error('Could not transform %s to %s: %s' % (from_frame, to_frame, ex))
            print('Could not transform %s to %s: %s' % (from_frame, to_frame, ex))
            return None
    
    def __del__(self):
        self.pipe.stop()

class JoyToRobot(Node):
    def __init__(self, camera, serverhandler):
        super().__init__('joy_to_twist_publisher')
        self.frame_to_publish = 'tool0'

        # Tf listener
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, self)

        # Camera instance
        self.camera = camera

        # Uploader instance
        self.serverhandler = serverhandler

        # Setup pub/sub control
        self.joy_sub = self.create_subscription(
            Joy, '/joy', self._joy_callback, 10)
        self.twist_pub = self.create_publisher(
            TwistStamped, '/servo_node/delta_twist_cmds', 10)
        self.collision_pub = self.create_publisher(
            PlanningScene, '/planning_scene', 10)
        # Setup pubs for cameras
        self.pc_pub = self.create_publisher(
            PointCloud2, '/camera/depth', 10)
        self.pcstream_pub = self.create_publisher(
            PointCloud2, '/camera/depth_stream', 10)
        self.color_pub = self.create_publisher(
            Image, '/color/image_raw', 10)
        self.camera_info_pub = self.create_publisher(
            CameraInfo, '/color/camera_info', 10)
        self.odometry_pub = self.create_publisher(
            Odometry, '/odom', 10)
        
        # Setting a timer to call the 'publish_messages' at 60hz
        timer_period = 0.016  # seconds
        self.timer = self.create_timer(timer_period, self._publish_messages)
        
        self.servo_start_client = self.create_client(Trigger, '/servo_node/start_servo')
        
        while not self.servo_start_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        request = Trigger.Request()
        future = self.servo_start_client.call_async(request)

        odom_msg = Odometry()
        odom_msg.header.stamp = self.get_clock().now().to_msg()
        odom_msg.header.frame_id = 'base_link' 
        odom_msg.child_frame_id = 'camera_link'  

        # Load the collision scene asynchronously
        self.collision_pub_thread = threading.Thread(target=self._load_collision_scene)
        self.collision_pub_thread.start()
        self.AXIS_DEFAULTS = {5: 1.0, 2: 1.0}

        self.lastcommand = time.time()
        print('Playstation mode initiated')
        print('--------------------------------')
        print('Cross: Take picture and betwixtnt saving')
        print('Circle: Save combined pointcloud')
        print('Triangle: Reset Scan')

    def _joy_callback(self, msg):
        # Create the messages we publish
        twist_msg = TwistStamped()

        # This call updates the frame for twist commands
        self._update_workmode(msg.axes)
        self._convert_joy_to_trajectory(msg.buttons)
        # Convert the joystick message to Twist or JointJog and publish
        if self._convert_joy_to_cmd(msg.axes, msg.buttons, twist_msg):
            # publish the TwistStamped
            twist_msg.header.frame_id = self.frame_to_publish
            twist_msg.header.stamp = self.get_clock().now().to_msg()
            self.twist_pub.publish(twist_msg)

        # Made it into threads
        #self.camera.camera_color_publish(self.color_pub, self.camera_info_pub)
        #self._publish_odom()
        #self.camera.publish_pc_stream(self.pcstream_pub, 'base_link', 'tool0', self.tf_buffer)

    def _convert_joy_to_cmd(self, axes, buttons, twist):
        # Camera functionality to the buttons
        if buttons[0]:
            if time.time() - self.lastcommand > 2:
                msg = self.camera.save_points(self.pc_pub, 'camera_link', 'base_link', self.tf_buffer, save=True) #camera, base_link
                self.lastcommand = time.time()
                print('Picture taken, saved as ' + str(msg))
        if buttons[1]:
            if time.time() - self.lastcommand > 2:
                msg = self.camera.save_points_as_ply('collected/combined.ply')
                self.lastcommand = time.time()
                print('Combined picture saved as' + msg)
        if buttons[2]:
            if time.time() - self.lastcommand > 2:
                self.camera.reset_points()
                self.lastcommand = time.time()
                print('Reset points')

        if buttons[3]:
            if time.time() - self.lastcommand > 2:
                self.serverhandler.upload('collected/combined.ply')

                #Åpne Pyvista?

                self.lastcommand = time.time()
                print('File uploaded')


        # Mapping buttons and axes to twist commands
        twist.twist.linear.z = axes[4] * 1
        twist.twist.linear.y = axes[3] * 1

        lin_x_right = -0.5 * (axes[5] - self.AXIS_DEFAULTS.get(5, 1.0))
        lin_x_left = 0.5 * (axes[2] - self.AXIS_DEFAULTS.get(2, 1.0))
        twist.twist.linear.x = lin_x_right + lin_x_left

        twist.twist.angular.y = axes[1]
        twist.twist.angular.x = axes[0]

        roll_positive = buttons[5]
        roll_negative = -1 * buttons[4]
        twist.twist.angular.z = float(roll_positive + roll_negative)
        return True

    def _convert_joy_to_trajectory(self, buttons):
        if buttons[9]:
            if time.time() - self.lastcommand > 2:
                self._call_trajectory()
                self.lastcommand = time.time()

        elif buttons[8]:
            if time.time() - self.lastcommand > 2:
                self._call_trajectory(True)
                self.lastcommand = time.time()

    def _update_workmode(self, axes):
        if (axes[7] < -0.1) and self.frame_to_publish == 'tool0':
            self.frame_to_publish = 'base_link'
        elif (axes[7] > 0.1) and self.frame_to_publish == 'base_link':
            self.frame_to_publish = 'tool0'
        elif (axes[6]< -0.1):
            if time.time() - self.lastcommand > 2:
                self.lastcommand = time.time()
                self._switch_controllers('scaled_joint_trajectory_controller', 'forward_position_controller')
        elif (axes[6] > 0.1):
            if time.time() - self.lastcommand > 2:
                self.lastcommand = time.time()
                self._switch_controllers('forward_position_controller', 'scaled_joint_trajectory_controller')

    def _switch_controllers(self,deactivate_controller, activate_controller):
        command = [
            "ros2", "control", "switch_controllers",
            "--deactivate", deactivate_controller,
            "--activate", activate_controller
        ]

        result = subprocess.run(command, capture_output=True, text=True)

        if result.returncode == 0:
            print("Successfully switched controllers.")
            print("Output:", result.stdout)
            print(activate_controller)
        else:
            print("Error in switching controllers.")
            print("Error:", result.stderr)

    def _call_trajectory(self, preview=False):

        self._convert_points('/home/kevin/workspacegl/Msc15/control/points.ply',
                            '/home/kevin/workspacegl/Msc15/control/points_output',
                            '')
        
        if preview:
            trajectory = 'preview_trajectory'
        else:
            trajectory = 'follow_trajectory'

        command = [
            "ros2", "run", "robot_control",
            trajectory
        ]

        result = subprocess.run(command, capture_output=True, text=True)

        if result.returncode == 0:
            print("Successfully previewed trajectory.")
            print("Output:", result.stdout)
        else:
            print("Error in previewing trajectory.")
            print("Error:", result.stderr)

    def _publish_messages(self):
        time = self.get_clock().now()
        stamp = time.to_msg()
        self._publish_odom(stamp)
        #self.camera.camera_color_publish(self.color_pub, self.camera_info_pub, stamp)
        #self.camera.publish_pc_stream(self.pcstream_pub, 'camera_link', 'base_link', self.tf_buffer, stamp)

    def _publish_odom(self, stamp):
        try:
            # Replace 'odom' and 'base_link' with your actual frame names as necessary
            try:
                now = rclpy.time.Time()
                transform = self.tf_buffer.lookup_transform('base_link', 'camera_link', now)
            except (LookupException, ConnectivityException, ExtrapolationException) as ex:
                self.get_logger().error('Could not transform %s to %s: %s' % ('camera_link', 'base_link', ex))
                return None
            # Create and fill the odometry message
            odom_msg = Odometry()
            odom_msg.header.stamp = stamp
            odom_msg.header.frame_id = 'base_link'
            odom_msg.child_frame_id = 'camera_link'

            # Set the pose (position + orientation) from the transform
            odom_msg.pose.pose.position.x = transform.transform.translation.x
            odom_msg.pose.pose.position.y = transform.transform.translation.y
            odom_msg.pose.pose.position.z = transform.transform.translation.z
            odom_msg.pose.pose.orientation = transform.transform.rotation

            # Publish the odometry message
            self.odometry_pub.publish(odom_msg)
            

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            self.get_logger().error('Error getting transform: %s' % e)

    def _convert_points(self, inputfile, outputfile, path):
        points = pv.read(inputfile)

        with open(str(path) + str(outputfile) + '.txt', 'w') as file:
            for each in points.points:
                file.write(str(each[0]) + ' ' + str(each[1]) + ' ' + str(each[2]) + ' ' + '\n')

    def _load_collision_scene(self):
        time.sleep(3)
        # Create a PlanningScene message
        planning_scene = PlanningScene()
        planning_scene.is_diff = True

        # Create collision object, in the way of servoing
        collision_object = CollisionObject()
        collision_object.id = 'box'

        # ... (same collision scene setup as in C++ code)

        planning_scene.world.collision_objects.append(collision_object)

        self.collision_pub.publish(planning_scene)
               
    def destroy(self):
        if self.collision_pub_thread.is_alive():
            self.collision_pub_thread.join()

class serverhandler():
    def __init__(self, uploadurl, downloadurl):
        self.uploadurl = str(uploadurl)
        self.downloadurl = str(downloadurl)

    def upload(self, file):
        files = {'file': open(file, 'rb')} 
        response = requests.post(self.uploadurl, files=files)

    def download(self, destination, file):
        response = requests.get(self.downloadurl + str(file))
        with open(str(destination), 'wb') as f:
            f.write(response.content)


