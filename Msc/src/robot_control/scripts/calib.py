import rclpy, tf2_ros
from rclpy.time import Time
from rclpy.qos import QoSProfile
from rclpy.node import Node
from sensor_msgs.msg import Joy, PointCloud2, PointField, Image, CameraInfo
from geometry_msgs.msg import TwistStamped, TransformStamped
from control_msgs.msg import JointJog
from std_srvs.srv import Trigger
from moveit_msgs.msg import PlanningScene, CollisionObject
import pyrealsense2 as rs
import threading, ros2_numpy, numpy as np, array, time, tf2_py as tf2, vedo, cv2, time
from cv_bridge import CvBridge
from tf2_ros import LookupException, ConnectivityException, ExtrapolationException
#from tf2_ros import quaternion, translation
#from tf.transformations.quaternion_matrix

class JoyToServoPub(Node):
    def __init__(self):
        super().__init__('joy_to_twist_publisher')
        self.frame_to_publish = 'tool0'

        # Setup pub/sub control
        self.joy_sub = self.create_subscription(
            Joy, '/joy', self.joy_callback, 100)
        self.twist_pub = self.create_publisher(
            TwistStamped, '/servo_node/delta_twist_cmds', 10)
        self.collision_pub = self.create_publisher(
            PlanningScene, '/planning_scene', 10)
        # Setup pubs for cameras
        self.color_pub = self.create_publisher(
            Image, 'color/image_raw', 10)
        self.camera_info_publisher = self.create_publisher(
            CameraInfo, 'color/camera_info', 10)

        
        # TF
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, self)
        
        # Camera
        self.pipe = rs.pipeline()
        self.cfg = rs.config()
        #self.cfg.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        self.cfg.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
        self.pc = rs.pointcloud()
        self.profile = self.pipe.start(self.cfg)
        self.intrinsics = self.profile.get_stream(rs.stream.color).as_video_stream_profile().get_intrinsics()
        self.temporal = rs.temporal_filter()
        self.decimation = rs.decimation_filter(magnitude = 5)

        self.msgCount = 1

        # Bridge for posting camera information easily
        self.bridge = CvBridge()

        # Camera info msg
        self.camera_info_msg = CameraInfo()
        self.camera_info_msg.width = self.intrinsics.width
        self.camera_info_msg.height = self.intrinsics.height
        self.camera_info_msg.distortion_model = 'plumb_bob'
        self.camera_info_msg.d = [0.0, 0.0, 0.0, 0.0, 0.0]  # Assuming no distortion
        self.camera_info_msg.k = [self.intrinsics.fx, 0.0, self.intrinsics.ppx,
                             0.0, self.intrinsics.fy, self.intrinsics.ppy,
                             0.0, 0.0, 1.0]
        self.camera_info_msg.r = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0]
        self.camera_info_msg.p = [self.intrinsics.fx, 0.0, self.intrinsics.ppx, 0.0,
                             0.0, self.intrinsics.fy, self.intrinsics.ppy, 0.0,
                             0.0, 0.0, 1.0, 0.0]

        # Load the collision scene asynchronously
        self.collision_pub_thread = threading.Thread(target=self.load_collision_scene)
        self.collision_pub_thread.start()
        self.AXIS_DEFAULTS = {5: 1.0, 2: 1.0}


        # Calibration variables
        self.R_gripper2base_list = []
        self.t_gripper2base_list = []
        self.R_target2cam_list = []
        self.t_target2cam_list = []


        self.lastcommand = time.time()
        print('calibration mode')
        print('--------------------------------')
        print('Cross: save state')
        print('Circle: save it all')
        time.sleep(4)
        print('4 sec over')


    def joy_callback(self, msg):
        # Create the messages we publish
        twist_msg = TwistStamped()

        # This call updates the frame for twist commands
        self.update_cmd_frame(msg.axes)
        # Convert the joystick message to Twist or JointJog and publish
        if self.convert_joy_to_cmd(msg.axes, msg.buttons, twist_msg):
            # publish the TwistStamped
            twist_msg.header.frame_id = self.frame_to_publish
            twist_msg.header.stamp = self.get_clock().now().to_msg()
            self.twist_pub.publish(twist_msg)

        self.camera_color()


    def convert_joy_to_cmd(self, axes, buttons, twist):
        # Camera functionality to the buttons
        if buttons[0]:
            if time.time() - self.lastcommand > 2:
                print('Scan number ' + str(self.msgCount))
                self.save()
                self.lastcommand = time.time()
                self.msgCount += 1
        if buttons[1]:
            if time.time() - self.lastcommand > 2:
                R, t = cv2.calibrateHandEye(self.R_gripper2base_list, self.t_gripper2base_list, self.R_target2cam_list, self.t_target2cam_list, method=0)
                print('rotational matrix')
                print(R)
                print('translational matrix')
                print(t)
                self.lastcommand = time.time()


        # Mapping buttons and axes to twist commands
        twist.twist.linear.z = axes[4]
        twist.twist.linear.y = axes[3]

        lin_x_right = -0.5 * (axes[5] - self.AXIS_DEFAULTS.get(5, 1.0))
        lin_x_left = 0.5 * (axes[2] - self.AXIS_DEFAULTS.get(2, 1.0))
        twist.twist.linear.x = lin_x_right + lin_x_left

        twist.twist.angular.y = axes[1]
        twist.twist.angular.x = axes[0]

        roll_positive = buttons[5]
        roll_negative = -1 * buttons[4]
        twist.twist.angular.z = float(roll_positive + roll_negative)
        return True
        



    def save(self):
        # Retrieve transform data
        gripper2base = self.get_transform('tool0', 'base_link') #camera, base_link with robot tf | marker_camera -> marker_x with markers
        target2cam= self.get_transform('aruco_marker_frame', 'camera_optical_frame')

        if (target2cam or gripper2base) is None:
            print('No transform')
            return  # or handle the error as appropriate
        
        R_gripper2base, t_gripper2base = self.quaternion_to_matrix(gripper2base)
        R_target2cam, t_target2cam = self.quaternion_to_matrix(target2cam)
        print('Gripper to Base rotation')
        print(R_gripper2base)
        print('Gripper to Base translation')
        print(t_gripper2base)
        print(R_target2cam)
        print('Target to camera rotation')
        print(t_target2cam)
        print('Target to camera translation')
        self.R_gripper2base_list.append(R_gripper2base)
        self.t_gripper2base_list.append(t_gripper2base)
        self.R_target2cam_list.append(R_target2cam)
        self.t_target2cam_list.append(t_target2cam)


    def camera_color(self):
        # Retrieve frame
        frame = self.pipe.wait_for_frames()
        color_frame = frame.get_color_frame()
        if not color_frame:
            return
        #If frame, convert to imgmsg and publish color frame
        color_image = np.asanyarray(color_frame.get_data())
        ros_image = self.bridge.cv2_to_imgmsg(color_image, "bgr8")
        self.color_pub.publish(ros_image)
        # Post camera info
        self.camera_info_msg.header = ros_image.header 
        self.camera_info_publisher.publish(self.camera_info_msg)





    def quaternion_to_matrix(self, transform):
        x, y, z, w = transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w
        xx, yy, zz, xy, xz, yz, wx, wy, wz = (x * x, y * y, z * z, x * y, x * z, y * z, w * x, w * y, w * z)

        rot_matrix = np.array([
            [1 - 2 * (yy + zz),     2 * (xy - wz),     2 * (xz + wy)],
            [    2 * (xy + wz), 1 - 2 * (xx + zz),     2 * (yz - wx)],
            [    2 * (xz - wy),     2 * (yz + wx), 1 - 2 * (xx + yy)]
        ])


        # Add the translation part
        trans_matrix = [transform.translation.x, transform.translation.y, transform.translation.z]
        trans_matrix = np.array(trans_matrix).reshape(-1, 1)

        return rot_matrix, trans_matrix
    


    def get_transform(self, from_frame, to_frame):
        try:
            now = rclpy.time.Time()
            trans = self.tf_buffer.lookup_transform(to_frame, from_frame, now)
            return trans.transform
        except (LookupException, ConnectivityException, ExtrapolationException) as ex:
            self.get_logger().error('Could not transform %s to %s: %s' % (from_frame, to_frame, ex))
            return None
        
    def update_cmd_frame(self, axes):
        if (axes[7] < -0.1) and self.frame_to_publish == 'tool0':
            self.frame_to_publish = 'base_link'
        elif (axes[7] > 0.1) and self.frame_to_publish == 'base_link':
            self.frame_to_publish = 'tool0'

    def load_collision_scene(self):
        time.sleep(3)
        # Create a PlanningScene message
        planning_scene = PlanningScene()
        planning_scene.is_diff = True

        # Create collision object, in the way of servoing
        collision_object = CollisionObject()
        collision_object.id = 'box'

        # ... (same collision scene setup as in C++ code)

        planning_scene.world.collision_objects.append(collision_object)

        self.collision_pub.publish(planning_scene)

    def destroy(self):
        if self.collision_pub_thread.is_alive():
            self.collision_pub_thread.join()
    
    def __del__(self):
        self.pipe.stop()



def main(args=None):
    rclpy.init(args=args)
    joy_to_servo_pub = JoyToServoPub()
    rclpy.spin(joy_to_servo_pub)
    joy_to_servo_pub.destroy()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
