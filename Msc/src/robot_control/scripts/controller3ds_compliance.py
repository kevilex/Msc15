#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PoseStamped, Quaternion, Vector3
from omni_msgs.msg import OmniState, OmniButtonEvent
import yaml
from math import fabs


class OmniStateToDeltaPose(Node):
    def __init__(self):
        super().__init__('omni_state_to_delta_pose')
        
        self.package_name = "touch_to_joy"
        
        self.movement_scale = 0.005
        self.deadband = 0
        self.consecutive_nonzero = 1
        
        self.omni_sub = self.create_subscription(
            OmniState, 'phantom/state', self.omni_callback, 10)
        self.button_sub = self.create_subscription(
            OmniButtonEvent, 'phantom/button', self.button_callback, 10)
        self.delta_pose_pub = self.create_publisher(
            PoseStamped, '/target_frame', 10)
        
        self.initial_pose = None
        self.initial_orientation = None
        self.movement_active = False

    def button_callback(self, msg):
        self.movement_active = bool(msg.grey_button)
        if self.movement_active:
            self.initial_pose = None
            self.initial_orientation = None

    def omni_callback(self, msg):
        callback_time = self.get_clock().now()
        
        if self.movement_active:
            if self.initial_pose is None:
                self.initial_pose = msg.pose
                self.initial_orientation = msg.pose.orientation
            else:
                delta_pose = PoseStamped()
                delta_pose.header.frame_id = 'base_link'
                delta_pose.header.stamp = callback_time.to_msg()
                
                delta_position = self.calculate_delta_position(self.initial_pose, msg.pose)
                delta_pose.pose.position.x = delta_position.x
                delta_pose.pose.position.y = delta_position.y
                delta_pose.pose.position.z = delta_position.z
                
                delta_orientation = self.calculate_delta_orientation(self.initial_orientation, msg.pose.orientation)
                delta_pose.pose.orientation = delta_orientation
                
                self.deadband_filter_noise(delta_pose, self.deadband)
                self.scale_delta_pose(delta_pose, self.movement_scale)
                self.delta_pose_pub.publish(delta_pose)

        self.get_logger().info('omniCallback Processing took %.5fms. Max processing time allowed is 1ms for 1000 hz.' % ((self.get_clock().now() - callback_time).nanoseconds / 1e6))
        
    def calculate_delta_position(self, initial_pose, current_pose):
        delta_position = Vector3()
        delta_position.x = current_pose.position.x - initial_pose.position.x
        delta_position.y = current_pose.position.y - initial_pose.position.y
        delta_position.z = current_pose.position.z - initial_pose.position.z
        return delta_position
    
    def calculate_delta_orientation(self, initial_orientation, current_orientation):
        # Assuming quaternion rotation
        # For simplicity, you might want to use other libraries like tf2_ros or transformations
        # to handle quaternion rotation operations
        delta_orientation = Quaternion()
        # In this example, we assume the initial orientation is the identity quaternion
        delta_orientation.x = current_orientation.x - initial_orientation.x
        delta_orientation.y = current_orientation.y - initial_orientation.y
        delta_orientation.z = current_orientation.z - initial_orientation.z
        delta_orientation.w = current_orientation.w - initial_orientation.w
        
        # Ensure that values are wrapped correctly
        if delta_orientation.x < -0.5:
            delta_orientation.x += 1.0
        elif delta_orientation.x > 0.5:
            delta_orientation.x -= 1.0
        if delta_orientation.y < -0.5:
            delta_orientation.y += 1.0
        elif delta_orientation.y > 0.5:
            delta_orientation.y -= 1.0
        if delta_orientation.z < -0.5:
            delta_orientation.z += 1.0
        elif delta_orientation.z > 0.5:
            delta_orientation.z -= 1.0
        if delta_orientation.w < -0.5:
            delta_orientation.w += 1.0
        elif delta_orientation.w > 0.5:
            delta_orientation.w -= 1.0

        return delta_orientation
        
    def deadband_filter_noise(self, delta_pose, deadband):
        delta_pose.pose.position.x = 0.0 if fabs(delta_pose.pose.position.x) < deadband else delta_pose.pose.position.x
        delta_pose.pose.position.y = 0.0 if fabs(delta_pose.pose.position.y) < deadband else delta_pose.pose.position.y
        delta_pose.pose.position.z = 0.0 if fabs(delta_pose.pose.position.z) < deadband else delta_pose.pose.position.z

    def scale_delta_pose(self, delta_pose, scale):
        delta_pose.pose.position.x *= scale
        delta_pose.pose.position.y *= scale
        delta_pose.pose.position.z *= scale

def main(args=None):
    rclpy.init(args=args)
    to_delta_pose = OmniStateToDeltaPose()
    rclpy.spin(to_delta_pose)
    to_delta_pose.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()