#!/usr/bin/env python3
import rclpy, subprocess, time, threading
from rclpy.node import Node
from sensor_msgs.msg import Joy
from geometry_msgs.msg import TwistStamped, PoseStamped, PoseArray, Pose
from control_msgs.msg import JointJog
from std_srvs.srv import Trigger
from moveit_msgs.msg import PlanningScene, CollisionObject, PlanningSceneComponents
from moveit_msgs.srv import GetPlanningScene
from tf2_ros import TransformListener, Buffer
from geometry_msgs.msg import TransformStamped


class JoyToServo(Node):
    def __init__(self, joytopic='joy', twistpubtopic='/servo_node/delta_twist_cmds',
                 jointpubtopic ='/servo_node/delta_joint_cmds',
                 startservotrigger='/servo_node/start_servo'):
        
        super().__init__('joy_to_twist_publisher')
        self.frame_to_publish = 'wrist_2_link'

        # Setup pub/sub
        self.joy_sub = self.create_subscription(
            Joy, joytopic, self.joy_callback, 15)
        self.twist_pub = self.create_publisher(
            TwistStamped, twistpubtopic, 15)
        self.joint_pub = self.create_publisher(
            JointJog, '/servo_node/delta_joint_cmds', 15)
        self.collision_pub = self.create_publisher(
            PlanningScene, '/planning_scene', 10)
        self.pose_array_pub = self.create_publisher(
            PoseArray, '/appended_pose_array', 10)  
        
        self.pose_array = PoseArray()
        self.pose_array.header.frame_id = 'base_link'  # Set frame ID for PoseArray

        
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)
        
        # Create a service client to start the ServoNode
        self.servo_start_client = self.create_client(Trigger, startservotrigger)
        while not self.servo_start_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        request = Trigger.Request()
        future = self.servo_start_client.call_async(request)

        # Load the collision scene asynchronously
        self.collision_pub_thread = threading.Thread(target=self.load_collision_scene)
        self.collision_pub_thread.start()
        self.AXIS_DEFAULTS = {5: 1.0, 2: 1.0}

        self.lastcommand = time.time()


    def joy_callback(self, msg):
        # Create message that we might publish
        twist_msg = TwistStamped()
        joint_msg = JointJog()

        # This call updates the frame for twist commands
        self._update_workmode(msg.axes)
        self._convert_joy_to_trajectory(msg.buttons)

        # Convert the joystick message to Twist
        self._convert_joy_to_cmd(msg.axes, msg.buttons, twist_msg, joint_msg)

        # publish the TwistStamped
        twist_msg.header.frame_id = self.frame_to_publish
        twist_msg.header.stamp = self.get_clock().now().to_msg()
        self.twist_pub.publish(twist_msg)
    
        # publish the JointJog
        joint_msg.header.stamp = self.get_clock().now().to_msg()
        joint_msg.header.frame_id = 'base_link'
        
        self.joint_pub.publish(joint_msg)

    def _convert_joy_to_cmd(self, axes, buttons, twist, joint): 
        if buttons[1]: # Circle
            joint.joint_names = ['shoulder_pan_joint']
            joint.velocities = [-5.0]  # Positive velocity for clockwise
        elif buttons[2]:  # Triangle
            joint.joint_names = ['shoulder_pan_joint']
            joint.velocities = [5.0]  # Negative velocity for counter-clockwise
        else:
            joint.joint_names = ['shoulder_pan_joint']
            joint.velocities = []
            
        # Mapping buttons and axes to twist commands
        twist.twist.linear.z = axes[4] * 1
        twist.twist.linear.x = -axes[3] * 1

        lin_x_right = -0.5 * (axes[5] - self.AXIS_DEFAULTS.get(5, 1.0))
        lin_x_left = 0.5 * (axes[2] - self.AXIS_DEFAULTS.get(2, 1.0))
        twist.twist.linear.y = lin_x_right + lin_x_left

        twist.twist.angular.z = axes[0]
        twist.twist.angular.x = axes[1]

        roll_positive = buttons[5]
        roll_negative = -1 * buttons[4]
        twist.twist.angular.y = float(roll_positive + roll_negative)
        return True
    
    def _convert_joy_to_trajectory(self, buttons):
        if buttons[9]:
            if time.time() - self.lastcommand > 2:
                self._call_trajectory(2)
                self.get_logger().info('Trajectory called')
                self.lastcommand = time.time()

        elif buttons[8]:
            if time.time() - self.lastcommand > 2:
                self._call_trajectory(1)
                self.get_logger().info('Preview called')
                self.lastcommand = time.time()

        elif buttons[0]:
            if time.time() - self.lastcommand > 2:
                self._appendpose()
                self.get_logger().info('Position saved')
                self.lastcommand = time.time()
        
        elif buttons[12]:
            if time.time() - self.lastcommand > 2:
                self._call_trajectory(3)
                self.get_logger().info('Preview called')
                self.lastcommand = time.time()
        
        


    def _appendpose(self):
        position, orientation = self.get_pose()
        if position is None or orientation is None:
            self.get_logger().error('Failed to get pose')   
            return
        
        position_str = f"{position[0]} {position[1]} {position[2]}\n"
        orientation_str = f"{orientation[0]} {orientation[1]} {orientation[2]} {orientation[3]}\n"

        with open('/home/kevin/workspacegl/Msc15/control/points_onthego.txt', 'a') as position_file:
            position_file.write(position_str)
        
        with open('/home/kevin/workspacegl/Msc15/control/orientation_onthego.txt', 'a') as orientation_file:
            orientation_file.write(orientation_str)

        pose = Pose()
        pose.position.x = position[0]
        pose.position.y = position[1]
        pose.position.z = position[2]
        pose.orientation.x = orientation[0]
        pose.orientation.y = orientation[1]
        pose.orientation.z = orientation[2]
        pose.orientation.w = orientation[3]
        
        self.pose_array.poses.append(pose) 

        # Publish the PoseArray
        self.pose_array.header.stamp = self.get_clock().now().to_msg()
        self.pose_array_pub.publish(self.pose_array)


    def get_pose(self):
        try:
            now = rclpy.time.Time()
            trans = self.tf_buffer.lookup_transform('base_link', 'tool0', now)

            position = [
                trans.transform.translation.x,
                trans.transform.translation.y,
                trans.transform.translation.z
            ]

            orientation = [
                trans.transform.rotation.x,
                trans.transform.rotation.y,
                trans.transform.rotation.z,
                trans.transform.rotation.w
            ]

            return position, orientation
        
        except Exception as e:
            self.get_logger().error(f'Could not transform: {e}')
            return None, None


    def _update_workmode(self, axes):
        if (axes[7] < -0.1) and self.frame_to_publish == 'base_link':
            self.frame_to_publish = 'wrist_2_link'
        elif (axes[7] > 0.1) and self.frame_to_publish == 'wrist_2_link':
            self.frame_to_publish = 'base_link'
        elif (axes[6] < -0.1):
            if time.time() - self.lastcommand > 2:
                self.lastcommand = time.time()
                self._switch_controllers(['scaled_joint_trajectory_controller', 'cartesian_motion_controller'], 'forward_position_controller')
        elif (axes[6] > 0.1):
            if time.time() - self.lastcommand > 2:
                self.lastcommand = time.time()
                self._switch_controllers(['forward_position_controller', 'cartesian_motion_controller'], 'scaled_joint_trajectory_controller')

    def _call_trajectory(self, traj):
        #self._convert_points('/home/kevin/workspacegl/Msc15/control/points.ply',
        #                    '/home/kevin/workspacegl/Msc15/control/points_output',
        #                    '')
        
        if traj == 1:
            trajectory = 'preview_trajectory'
        elif traj == 2:
            trajectory = 'follow_trajectory'
        elif traj == 3:
            trajectory = 'preview_onthego'
        elif traj == 4:
            trajectory = 'follow_onthego'

        command = [
            "ros2", "run", "robot_control",
            trajectory
        ]

        result = subprocess.run(command, capture_output=True, text=True)

        if result.returncode == 0:
            print("Successfully previewed trajectory.")
            print("Output:", result.stdout)
        else:
            print("Error in previewing trajectory.")
            print("Error:", result.stderr)

    def _switch_controllers(self,deactivate_controller, activate_controller):
        command1 = [
            "ros2", "control", "switch_controllers",
            "--deactivate", deactivate_controller[1],
            "--activate", activate_controller
        ]
        command2 = [
            "ros2", "control", "switch_controllers",
            "--deactivate", deactivate_controller[0],
            "--activate", activate_controller
        ]

        anti_aggression = [
            "ros2", "service", "call", "/servo_node/start_servo", "std_srvs/srv/Trigger", "{}"
        ]
        
        anti_aggression_result = subprocess.run(anti_aggression, capture_output=True, text=True)
        result1 = subprocess.run(command1, capture_output=True, text=True)
        result2 = subprocess.run(command2, capture_output=True, text=True)

        if result1.returncode == 0 or result2.returncode == 0:
            print("Successfully switched controllers.")
            print(activate_controller)
        else:
            print("Error in switching controllers.")

    def load_collision_scene(self):
        time.sleep(3)
        # Create a PlanningScene message
        planning_scene = PlanningScene()
        planning_scene.is_diff = True

        # Create collision object, in the way of servoing
        collision_object = CollisionObject()
        collision_object.id = 'box'

        # ... (same collision scene setup as in C++ code)

        planning_scene.world.collision_objects.append(collision_object)

        self.collision_pub.publish(planning_scene)

    def destroy(self):
        if self.collision_pub_thread.is_alive():
            self.collision_pub_thread.join()


def main(args=None):
    rclpy.init(args=args)
    joy_to_servo = JoyToServo()
    rclpy.spin(joy_to_servo)
    joy_to_servo.destroy()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
