#!/usr/bin/env python3
import rclpy, tf2_ros
from rclpy.node import Node
from nav_msgs.msg import Odometry 
from tf2_ros import LookupException, ConnectivityException, ExtrapolationException, TransformException
import serial, tf2_geometry_msgs
import tf2_geometry_msgs
from geometry_msgs.msg import TransformStamped
from tf_transformations import euler_from_quaternion, quaternion_from_euler
import time, math
class TFToOdom(Node):
    def __init__(self):
        super().__init__('odometry_publisher')

        self.odometry_pub = self.create_publisher(Odometry, '/odom', 10)
        self.tf_broadcaster = tf2_ros.TransformBroadcaster(self)

        # Serial port setup
        self.serial_port = serial.Serial('/dev/ttyUSB0', 115200) 
        self.serial_port.timeout=0.01
        self.timeout = 0.01
        # Timer setup for publishing odometry
        timer_period = 1.0 / 30  # 30Hz publishing frequency
        self.timer = self.create_timer(timer_period, self._publish_odom)

    def _publish_odom(self):
        try:
            bytesToRead = self.serial_port.inWaiting()
            angle_data = self.serial_port.read(bytesToRead).decode().strip()
            #print(angle_data)
            if angle_data:
                angle_data = angle_data.split('\r\n')[-2]  # Get the last value from the string
                rpy = [0.0, 0.0, -(float(angle_data)/25600)*2*math.pi] 
                print(float(angle_data)/25600*360)
                quaternion = quaternion_from_euler(rpy[0], rpy[1], rpy[2])

                time = self.get_clock().now()
                stamp = time.to_msg()
                odom_msg = Odometry()
                odom_msg.header.stamp = stamp
                odom_msg.header.frame_id = 'world'
                odom_msg.child_frame_id = 'camera_link'
                odom_msg.pose.pose.orientation.x = quaternion[0]
                odom_msg.pose.pose.orientation.y = quaternion[1]
                odom_msg.pose.pose.orientation.z = quaternion[2]
                odom_msg.pose.pose.orientation.w = quaternion[3]

                # Publish the odometry message
                self.odometry_pub.publish(odom_msg)

                 # Publish the TF transform
                transform_stamped = TransformStamped()
                transform_stamped.header.stamp = stamp
                transform_stamped.header.frame_id = 'world'
                transform_stamped.child_frame_id = 'camera_link'
                transform_stamped.transform.translation.x = 0.0  
                transform_stamped.transform.translation.y = 0.0
                transform_stamped.transform.translation.z = 0.0
                transform_stamped.transform.rotation.x = quaternion[0]
                transform_stamped.transform.rotation.y = quaternion[1]
                transform_stamped.transform.rotation.z = quaternion[2]
                transform_stamped.transform.rotation.w = quaternion[3]

                self.tf_broadcaster.sendTransform(transform_stamped)

        except Exception as t:
            print(t)
            pass
        
        except Exception as e:
            self.get_logger().error('Error: %s' % e)




def main(args=None):
    rclpy.init(args=args)
    odompub = TFToOdom()
    rclpy.spin(odompub)
    odompub.destroy()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
