
import numpy as np

path_orientation = "/home/kevin/workspacegl/Msc15/control/orientation_output.txt"

# Define the path to save the points
path_point = "/home/kevin/workspacegl/Msc15/control/points_output.txt"

# Parameters for the number eight
num_points = 100  # Number of points per circle
radius = 1.0  # Radius of each circle
z_offset = 0.0  # Z-coordinate offset for all points
center1 = (0.0, 1.0, z_offset)  # Center of the top circle
center2 = (0.0, -1.0, z_offset)  # Center of the bottom circle

# Generate points for the top circle
theta1 = np.linspace(0, 2 * np.pi, num_points)
circle1 = [(center1[0] + radius * np.cos(t), center1[1] + radius * np.sin(t), center1[2]) for t in theta1]

# Generate points for the bottom circle
theta2 = np.linspace(0, 2 * np.pi, num_points)
circle2 = [(center2[0] + radius * np.cos(t), center2[1] + radius * np.sin(t), center2[2]) for t in theta2]

# Combine the points
points = circle1 + circle2



# Save the points to the file
with open(path_point, 'w') as file:
    for point in points:
        file.write(f"{point[0]} {point[1]} {point[2]}\n")

print(f"Points saved to {path_point}")
