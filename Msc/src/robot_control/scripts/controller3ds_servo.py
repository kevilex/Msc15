#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import TwistStamped, Quaternion, Vector3
from omni_msgs.msg import OmniState, OmniButtonEvent
import yaml
from math import fabs


class OmniStateToTwist(Node):
    def __init__(self):
        super().__init__('omni_state_to_twist')
        
        self.package_name = "touch_to_joy"
        
        self.movement_scale = 0.005
        self.deadband = 0
        self.consecutive_nonzero =  1
    
        
        self.omni_sub = self.create_subscription(
            OmniState, 'phantom/state', self.omni_callback, 10)
        self.button_sub = self.create_subscription(
            OmniButtonEvent, 'phantom/button', self.button_callback, 10)
        self.twist_pub = self.create_publisher(
            TwistStamped, 'servo_node/delta_twist_cmds', 10)
        
        self.previous_proposed_twists = []
        
        # Initialize some variables
        self.last_processed_time = self.get_clock().now()
        self.last_orientation = Quaternion()
        self.movement_active = False

    def button_callback(self, msg):
        self.movement_active = bool(msg.grey_button)

    def omni_callback(self, msg):
        callback_time = self.get_clock().now()
        
        if self.movement_active:
            twist = TwistStamped()
            twist.header.frame_id = 'wrist_2_link'
            twist.header.stamp = callback_time.to_msg()
            dt = (callback_time - self.last_processed_time).nanoseconds / 1e9  # in seconds
            twist.twist.linear.x = msg.velocity.x
            twist.twist.linear.y = msg.velocity.y
            twist.twist.linear.z = msg.velocity.z
            angular = self.quaternion_poses_to_angular_velocity(self.last_orientation, msg.pose.orientation, dt)
            twist.twist.angular.x = angular.x
            twist.twist.angular.y = angular.y
            twist.twist.angular.z = angular.z
            self.deadband_filter_noise(twist, self.deadband)
            self.previous_movement_filter_noise(twist, self.consecutive_nonzero)
            self.scale_twist(twist, self.movement_scale)
            self.transform_twist_to_ur_frame(twist)
            self.twist_pub.publish(twist)

        self.get_logger().info('omniCallback Processing took %.5fms. Max processing time allowed is 1ms for 1000 hz.' % ((self.get_clock().now() - callback_time).nanoseconds / 1e6))
        self.last_processed_time = callback_time
        self.last_orientation = msg.pose.orientation

    def quaternion_poses_to_angular_velocity(self, q1, q2, dt):
        angular_velocity = Vector3()
        angular_velocity.x = (2.0 / dt) * (q1.w * q2.x - q1.x * q2.w - q1.y * q2.z + q1.z * q2.y)
        angular_velocity.y = (2.0 / dt) * (q1.w * q2.y + q1.x * q2.z - q1.y * q2.w - q1.z * q2.x)
        angular_velocity.z = (2.0 / dt) * (q1.w * q2.z - q1.x * q2.y + q1.y * q2.x - q1.z * q2.w)
        print(q2)
        return angular_velocity
    
    def deadband_filter_noise(self, twist, deadband):
        twist.twist.linear.x = 0.0 if fabs(twist.twist.linear.x) < deadband else twist.twist.linear.x
        twist.twist.linear.y = 0.0 if fabs(twist.twist.linear.y) < deadband else twist.twist.linear.y
        twist.twist.linear.z = 0.0 if fabs(twist.twist.linear.z) < deadband else twist.twist.linear.z
        twist.twist.angular.x = 0.0 if fabs(twist.twist.angular.x) < deadband else twist.twist.angular.x
        twist.twist.angular.y = 0.0 if fabs(twist.twist.angular.y) < deadband else twist.twist.angular.y
        twist.twist.angular.z = 0.0 if fabs(twist.twist.angular.z) < deadband else twist.twist.angular.z

    def previous_movement_filter_noise(self, twist, consecutive_nonzero):
        for prev in self.previous_proposed_twists:
            if prev.twist.linear.x == 0.0:
                twist.twist.linear.x = 0.0
            if prev.twist.linear.y == 0.0:
                twist.twist.linear.y = 0.0
            if prev.twist.linear.z == 0.0:
                twist.twist.linear.z = 0.0
            if prev.twist.angular.x == 0.0:
                twist.twist.angular.x = 0.0
            if prev.twist.angular.y == 0.0:
                twist.twist.angular.y = 0.0
            if prev.twist.angular.z == 0.0:
                twist.twist.angular.z = 0.0

        self.previous_proposed_twists.append(twist)
        if len(self.previous_proposed_twists) > consecutive_nonzero:
            del self.previous_proposed_twists[0]

    def scale_twist(self, twist, scale):
        twist.twist.linear.x *= scale
        twist.twist.linear.y *= scale
        twist.twist.linear.z *= scale
        twist.twist.angular.x *= scale
        twist.twist.angular.y *= scale
        twist.twist.angular.z *= scale

    def transform_twist_to_ur_frame(self, twist):
        temp_x = twist.twist.linear.x
        temp_y = twist.twist.linear.y

        twist.twist.linear.x = -temp_y
        twist.twist.linear.y = temp_x

def main(args=None):
    rclpy.init(args=args)
    to_twist = OmniStateToTwist()
    rclpy.spin(to_twist)
    to_twist.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
