#!/usr/bin/env python3
import rclpy, tf2_ros
from rclpy.node import Node
from nav_msgs.msg import Odometry 
from tf2_ros import LookupException, ConnectivityException, ExtrapolationException


class TFToOdom(Node):
    def __init__(self):
        super().__init__('odometry_publisher')

        # Tf listener
        self.tf_buffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tf_buffer, self)

        self.odometry_pub = self.create_publisher(
            Odometry, '/odom', 10)
        
        
        timer_period = 0.035087719  # 
        self.timer = self.create_timer(timer_period, self._publish_odom)

    def _publish_odom(self):
        try:
            try:
                now = rclpy.time.Time()
                transform = self.tf_buffer.lookup_transform('base_link', 'camera_link', now)
            except (LookupException, ConnectivityException, ExtrapolationException) as ex:
                self.get_logger().error('Could not transform %s to %s: %s' % ('camera_link', 'base_link', ex))
                return None
            
            # Create and fill the odometry message
            time = self.get_clock().now()
            stamp = time.to_msg()
            odom_msg = Odometry()
            odom_msg.header.stamp = stamp
            odom_msg.header.frame_id = 'base_link'
            odom_msg.child_frame_id = 'camera_link'

            # Set the pose (position + orientation) from the transform
            odom_msg.pose.pose.position.x = transform.transform.translation.x
            odom_msg.pose.pose.position.y = transform.transform.translation.y
            odom_msg.pose.pose.position.z = transform.transform.translation.z
            odom_msg.pose.pose.orientation = transform.transform.rotation

            # Publish the odometry message
            self.odometry_pub.publish(odom_msg)
            

        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            self.get_logger().error('Error getting transform: %s' % e)


def main(args=None):
    rclpy.init(args=args)
    odompub = TFToOdom()
    rclpy.spin(odompub)
    odompub.destroy()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
