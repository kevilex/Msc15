.. Robot documentation master file, created by
   sphinx-quickstart on Thu Feb 24 20:20:05 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the master thesis documentation
=================================

.. image:: src/fig/usecaseflowchart.png
 

.. toctree::
   :maxdepth: 5
   :caption: Contents:

   src/install
   src/robot_connection
   src/robot_usage
   src/camera_setup
   src/rtabmap_setup

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
