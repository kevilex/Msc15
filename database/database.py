from flask import Flask, request, jsonify, current_app
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename
import os
from flask_cors import CORS


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///items.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app) 
CORS(app)

CORS(app, resources={r"/api/*": {"origins": "http://localhost:3000"}})


# Define the Item model
class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    platform = db.Column(db.String(80), nullable=False)
    path = db.Column(db.String(80), nullable=False)
    imageSrc = db.Column(db.String(120), nullable=True)
    date = db.Column(db.String(80), nullable=False)
    pointcloud = db.Column(db.String(120), nullable=True)  
    mesh = db.Column(db.String(120), nullable=True)        


    def __repr__(self):
        return '<Item %r>' % self.name

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'platform': self.platform,
            'path': self.path,
            'imageSrc': self.imageSrc,
            'date': self.date,
            'pointcloud': self.pointcloud,
            'mesh': self.mesh
        }

# Initialize the database
with app.app_context():
    db.create_all()

app.config['UPLOAD_FOLDER'] = '/home/kevin/workspacegl/Msc15/web-app/public/ply'
@app.route('/items', methods=['POST'])
def add_item():
    data = request.form  # Get non-file data
    
    # Create new item with initial data (without file paths initially)
    new_item = Item(
        name=data['name'],
        platform=data['platform'],
        imageSrc= 'pic/' + str(data.get('platform')) + '.jpeg',
        path='',
        date = data['date']
    )


    # Add and commit to obtain the ID
    db.session.add(new_item)
    db.session.commit()
    new_item.path = 'ply/' + str(new_item.id) + '/'

    # Get files
    pointcloud_file = request.files['pointcloud']
    mesh_file = request.files['mesh']

    # Ensure filenames are secure
    pointcloud_filename = secure_filename(pointcloud_file.filename)
    mesh_filename = secure_filename(mesh_file.filename)

    # Define directory based on item ID
    item_directory = os.path.join(current_app.config['UPLOAD_FOLDER'], str(new_item.id))
    if not os.path.exists(item_directory):
        os.makedirs(item_directory)

    # Define where to save the files
    pointcloud_path = os.path.join(item_directory, pointcloud_filename)
    mesh_path = os.path.join(item_directory, mesh_filename)

    # Save the files
    pointcloud_file.save(pointcloud_path)
    mesh_file.save(mesh_path)

    # Update item with file paths
    new_item.pointcloud = pointcloud_path
    new_item.mesh = mesh_path
    db.session.commit()

    return jsonify(new_item.to_dict()), 201


# Route to get all items
@app.route('/items', methods=['GET'])
def get_items():
    items = Item.query.all()
    return jsonify([item.to_dict() for item in items])

if __name__ == '__main__':
    app.run(debug=True)
