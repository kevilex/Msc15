import requests

# URL of the `/items` endpoint in your Flask application
url = "http://localhost:5000/add_sub_item"

# Data for the new item
item_data = {
    "name": "Grane",
    "path": "grane",
    "imageSrc": "/pic/grane.jpeg"
}

# Files to be uploaded
files = {
    'pointcloud': open('points.ply', 'rb'),  # Update with actual file path
    'mesh': open('mesh.ply', 'rb')              # Update with actual file path
}

# Make a POST request to the Flask app with files and data
response = requests.post(url, data=item_data, files=files)

# Check the response
if response.status_code == 201:
    print("Item added successfully.")
    print("Response data:", response.json())
else:
    print(f"Failed to add item. Status code: {response.status_code}")
    print("Response:", response.text)

# Make sure to close the files after the request is made
files['pointcloud'].close()
files['mesh'].close()