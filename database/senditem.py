import requests

# URL of the `/items` endpoint in the flask application
url = "http://localhost:5000/items"

# Data for the new item
item_data = {
    "name": "Pressure tank",
    "platform": "Grane",
    "date" : "22.10.2000"
}

# Files to be uploaded
files = {
    'pointcloud': open('points.ply', 'rb'),  
    'mesh': open('mesh.ply', 'rb')              
}

# Make a POST request to the Flask app with files and data
response = requests.post(url, data=item_data, files=files)

# Check the response
if response.status_code == 201:
    print("Item added successfully.")
    print("Response data:", response.json())
else:
    print(f"Failed to add item. Status code: {response.status_code}")
    print("Response:", response.text)

# Close files after request
files['pointcloud'].close()
files['mesh'].close()