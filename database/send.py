import requests

url = 'http://localhost:5000/upload-item-with-subitems'

item_data = {
    'name': 'grane',
    'path': 'grane',
    'imageSrc': '/pic/grane.jpeg',
}

# Assuming sub_item_files structure is correct and contains the file objects
sub_item_files = {
    'subitem_0_mesh': open('mesh.ply', 'rb'),
    'subitem_0_points': open('points.ply', 'rb'),
    'subitem_1_mesh': open('mesh.ply', 'rb'),
    'subitem_1_points': open('points.ply', 'rb'),
}

# Structuring the sub-item data to match expected keys in Flask route
sub_item_data = {
    'subitem_0_name': 'Pressure Tank 1',
    'subitem_0_date': '2023-03-15',
    'subitem_1_name': 'Pressure Tank 2',
    'subitem_1_date': '2023-03-16',
}

# Combine item and sub-item data
data = {**item_data, **sub_item_data}

response = requests.post(url, data=data, files=sub_item_files)

if response.status_code == 201:
    print('Success:', response.json())
else:
    print('Failed:', response.text)

# Don't forget to close the files
for file in sub_item_files.values():
    file.close()
