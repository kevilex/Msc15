Camera usage
==================

Launching the RealSense Cameras
------------------------------

The realsense2_package is used.

.. code-block:: bash

    ros2 launch realsense2_camera rs_launch.py 

The arguments that should be used with this command are the following:

#. align_depth.enable - Makes the depth image and rgb image align with each other (true)
#. pointcloud.enable - Enables the stream of pointcloud to topic (true)
#. rgb_camera.profile - Sets resolution and fps of the color image stream ('640,480,30')
#. depth_module.profile - Sets resolution and fps of the depth image stream, could be helpful for faster ICP if odometry is failing ('640,480,30')
#. depth_module.gain - Sets power of laser, set lower if somewhat reflective surface, higher = better most of the time
#. temporal.filter.enable - Enables temporal filtering, should not be enabled if one does not have intentions of changing parameters

The general way of launching is therefore:

.. code-block:: bash

    ros2 launch realsense2_camera rs_launch.py align_depth.enable:=true pointcloud.enable:=true rgb_camera.profile:='640,480,30' depth_module.profile:='640,480,30' depth_module.gain:=2

After having launched the camera, one could change parameters of the camera. This helps with time-alignment for the depth, motion and rgb module with RTAB-Map, aswell as setting minimum distances to lower or higher values.

.. code-block:: bash
    
    ros2 param set /camera/camera depth_module.global_time_enabled true;
    ros2 param set /camera/camera motion_module.global_time_enabled true;
    ros2 param set /camera/camera rgb_camera.global_time_enabled true;
    ros2 param set /camera/camera depth_module.min_distance 100



In hand calibration of cameras
------------------------------

Hopefully, the rotational and translational arguments from the camera to the tool-end is identifiable through 3D-cad software and can just be posted into the real.launch.py file.

If this is not the case, here is how one could get a pretty accurate estimate by using a computational method by using the aruco_ros ros package.


#. Make the robot controllable by using real.launch.py
#. Launch the camera node by using the command shown above
#. Set the global_time_enable to true as shown above
#. Download aruco_ros
#. Change the aruco_ros single.launch file found in /opt/ros/humble/share/aruco_ros/launch
#. Launch the aruco node
#. Run calib.py found in the robot_control package and press x to gather transformations, press O in order to calculate the translation and rotation
#. Paste translation and rotation into real.launch.py for the correct camera.

How to download aruco_ros
^^^^^^^^^^^^^^^^^^^^^^^^
Just run this:

.. code-block:: bash

    sudo apt install ros-humble-aruco*

What to change in the single.launch.py file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: python

    from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, OpaqueFunction
from launch.substitutions import LaunchConfiguration
from launch.utilities import perform_substitutions
from launch_ros.actions import Node


def launch_setup(context, *args, **kwargs):

    eye = perform_substitutions(context, [LaunchConfiguration('eye')])

    aruco_single_params = {
        'image_is_rectified': True,
        'marker_size': LaunchConfiguration('marker_size'),
        'marker_id': LaunchConfiguration('marker_id'),
        'reference_frame': LaunchConfiguration('reference_frame'),
        'camera_frame': 'camera_optical_frame',
        'marker_frame': LaunchConfiguration('marker_frame'),
        'corner_refinement': LaunchConfiguration('corner_refinement'),
    }

    aruco_single = Node(
        package='aruco_ros',
        executable='single',
        parameters=[aruco_single_params],
        remappings=[('/camera_info', '/color/camera_info'),
                    ('/image', '/color/image_raw')],
    )

    return [aruco_single]


def generate_launch_description():

    marker_id_arg = DeclareLaunchArgument(
        'marker_id', default_value='300',
        description='Marker ID. '
    )

    marker_size_arg = DeclareLaunchArgument(
        'marker_size', default_value='0.10',
        description='Marker size in m. '
    )

    eye_arg = DeclareLaunchArgument(
        'eye', default_value='left',
        description='Eye. ',
        choices=['left', 'right'],
    )

    marker_frame_arg = DeclareLaunchArgument(
        'marker_frame', default_value='aruco_marker_frame',
        description='Frame in which the marker pose will be refered. '
    )

    reference_frame = DeclareLaunchArgument(
        'reference_frame', default_value='',
        description='Reference frame. '
        'Leave it empty and the pose will be published wrt param parent_name. '
    )

    corner_refinement_arg = DeclareLaunchArgument(
        'corner_refinement', default_value='LINES',
        description='Corner Refinement. ',
        choices=['NONE', 'HARRIS', 'LINES', 'SUBPIX'],
    )

    # Create the launch description and populate
    ld = LaunchDescription()

    ld.add_action(marker_id_arg)
    ld.add_action(marker_size_arg)
    ld.add_action(eye_arg)
    ld.add_action(marker_frame_arg)
    ld.add_action(reference_frame)
    ld.add_action(corner_refinement_arg)

    ld.add_action(OpaqueFunction(function=launch_setup))

    return ld

However, if this gets outdated, the important part of this launch file is the remappings, so set them to this when using intel realsense cameras:

.. code-block:: python

        aruco_single = Node(
        package='aruco_ros',
        executable='single',
        parameters=[aruco_single_params],
        remappings=[('/camera_info', '/color/camera_info'),
                    ('/image', '/color/image_raw')],
    )

How to launch the aruco node
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You need to specify the marker_id that you have printed and the marker size(meters) to your specifications.

.. code-block:: bash

    ros2 launch aruco_ros single.launch.py marker_id:=200 marker_size:=0.243

