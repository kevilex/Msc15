Install
==================

Preperations
--------
This project was developed on ubuntu 22.04. The desktop image for the operating system can be found `here <https://releases.ubuntu.com/jammy/>`_.

.. code-block:: console
    
    https://releases.ubuntu.com/jammy/


ROS install (taken from the `docs <https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html>`_)
----------

Set locale
----------

Make sure you have a locale which supports ``UTF-8``.
If you are in a minimal environment (such as a docker container), the locale may be something minimal like ``POSIX``.
We test with the following settings. However, it should be fine if you're using a different UTF-8 supported locale.

.. code-block:: bash

   locale  # check for UTF-8

   sudo apt update && sudo apt install locales
   sudo locale-gen en_US en_US.UTF-8
   sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
   export LANG=en_US.UTF-8

   locale  # verify settings

Setup Sources
-------------

You will need to add the ROS 2 apt repository to your system.

First ensure that the `Ubuntu Universe repository <https://help.ubuntu.com/community/Repositories/Ubuntu>`_ is enabled.

.. code-block:: bash

   sudo apt install software-properties-common
   sudo add-apt-repository universe

Now add the ROS 2 GPG key with apt.

.. code-block:: bash

   sudo apt update && sudo apt install curl -y
   sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg

Then add the repository to your sources list.

.. code-block:: bash

   echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

Install ROS 2 packages
----------------------

Update your apt repository caches after setting up the repositories.

.. code-block:: bash

   sudo apt update

.. include:: _Apt-Upgrade-Admonition.rst

.. warning::

   Due to early updates in Ubuntu 22.04 it is important that ``systemd`` and ``udev``-related packages are updated before installing ROS 2.
   The installation of ROS 2's dependencies on a freshly installed system without upgrading can trigger the **removal of critical system packages**.

   Please refer to `ros2/ros2#1272 <https://github.com/ros2/ros2/issues/1272>`_ and `Launchpad #1974196 <https://bugs.launchpad.net/ubuntu/+source/systemd/+bug/1974196>`_ for more information.

Desktop Install: ROS, RViz, demos, tutorials.

.. code-block:: bash

   sudo apt install ros-humble-desktop


Development tools: Compilers and other tools to build ROS packages

.. code-block:: bash

   sudo apt install ros-dev-tools

Environment setup
-----------------

Sourcing the setup script
^^^^^^^^^^^^^^^^^^^^^^^^^

Set up your environment by sourcing in the bashrc file.

.. code-block:: bash

   echo 'source /opt/ros/humble/setup.bash' >> ~/.bashrc



Clone and build the master thesis environment
------------

Clone repository

.. code-block::bash

    git clone https://gitlab.com/kevilex/Msc15.git


Navigate to the workspace folder

.. code-block::bash

    cd Msc/

Run rosdep in order to retrieve the necessary dependencies.

.. code-block::bash

    sudo rosdep update
    sudo rosdep install --from-paths src --ignore-src

Build the workspace. This is where errors occour that is related to dependencies which wasn't picked up by rosdep, chatgpt is good at handling these.
The MAKEFLAGS argument before the build states that the build can only use 4 cores, so that the computer doesnt bog itself in the attempt to be faster.

.. code-block::bash
    
    MAKEFLAGS-"j4" colcon build 

Source the workspace. If you would like to be able to use the packages as default in your command window, source in bashrc.

.. code-block::bash

    source install/setup.bash