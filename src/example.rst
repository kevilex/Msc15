Example Page
==================

Header
------
Math with LaTeX syntax

.. math::
	r_{g,0} = [x, y, z]^T + R_x(\phi) R_y(\theta) R_z(\psi) [0, 0, -d_g]^T
	
Figure
	
.. figure:: fig/uiabot2.png



List

#. Odometry
#. Slam
#. Transform frames

Code block console(/yaml/python)

.. code-block:: console

    ros2 run tf2_tools view_frames

Embedded youtube video

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/_liRh3FpHn0?si=gQJjgAKtZl5KU5CM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

