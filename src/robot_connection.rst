Robot Setup
==================

.. figure:: fig/UR5.png


Installing a URCap on a e-Series robot (taken from the `docs <https://docs.ros.org/en/ros2_packages/rolling/api/ur_robot_driver/user_docs/installation/install_urcap_e_series.html#install-urcap-e-series>`_)
------


For using the *ur_robot_driver* with a real robot you need to install the
**externalcontrol-1.0.5.urcap** which can be found inside the **resources** folder of this repo.

**Note**\ : For installing this URCap a minimal PolyScope version of 5.1 is necessary.

To install it you first have to copy it to the robot's **programs** folder which can be done either
via scp or using a USB stick.

On the welcome screen click on the hamburger menu in the top-right corner and select *Settings* to enter the robot's setup.  There select *System* and then *URCaps* to enter the URCaps installation screen.


.. image:: fig/es_01_welcome1.png
   :target: fig/es_01_welcome.png
   :alt: Welcome screen of an e-Series robot


There, click the little plus sign at the bottom to open the file selector. There you should see
all urcap files stored inside the robot's programs folder or a plugged USB drive.  Select and open
the **externalcontrol-1.0.5.urcap** file and click *open*. Your URCaps view should now show the
**External Control** in the list of active URCaps and a notification to restart the robot. Do that
now.


.. image:: fig/es_05_urcaps_installed.png
   :target: fig/es_05_urcaps_installed.png
   :alt: URCaps screen with installed urcaps


After the reboot you should find the **External Control** URCaps inside the *Installation* section.
For this select *Program Robot* on the welcome screen, select the *Installation* tab and select
**External Control** from the list.


.. image:: fig/es_07_installation_excontrol.png
   :target: src/fig/es_07_installation_excontrol.png
   :alt: Installation screen of URCaps


Here you'll have to setup the IP address of the external PC which will be running the ROS driver.
Note that the robot and the external PC have to be in the same network, ideally in a direct
connection with each other to minimize network disturbances. The custom port should be left
untouched for now.


.. image:: fig/es_10_prog_structure_urcaps.png
   :target: src/fig/es_10_prog_structure_urcaps.png
   :alt: Insert the external control node


To use the new URCaps, create a new program and insert the **External Control** program node into
the program tree


.. image:: fig/es_11_program_view_excontrol.png
   :target: src/fig/es_11_program_view_excontrol.png
   :alt: Program view of external control


If you click on the *command* tab again, you'll see the settings entered inside the *Installation*.
Check that they are correct, then save the program. Your robot is now ready to be used together with
this driver.


Network setup
-------------

There are many possible ways to connect a UR robot. This section describes a good example using static IP addresses and a direct connection from the PC to the Robot to minimize latency introduced by network hardware. Though a good network switch usually works fine, as well.


#.
   Connect the UR control box directly to the remote PC with an ethernet cable.

#.
   Open the network settings from the UR teach pendant (Setup Robot -> Network) and enter these settings:

.. code-block::

   IP address: 192.168.1.102
   Subnet mask: 255.255.255.0
   Default gateway: 192.168.1.1
   Preferred DNS server: 192.168.1.1
   Alternative DNS server: 0.0.0.0


#.
   On the remote PC, turn off all network devices except the "wired connection", e.g. turn off wifi.

#.
   Open Network Settings and create a new Wired connection with these settings. You may want to name this new connection ``UR`` or something similar:

.. code-block::

   IPv4
   Manual
   Address: 192.168.1.101
   Netmask: 255.255.255.0
   Gateway: 192.168.1.1


#. Verify the connection from the PC with e.g. ping.

.. code-block::

   ping 192.168.1.102
