Using RTAB-Map
==================

When the robot and camera has been launched, the RTAB-Map package can be used.

Launching RTAB-Map can be done through the robot_control package, either through the real.launch.py by using the rtab:=true argument or by launching it with rtab.launch.py.

.. code-block:: python

    ros2 launch robot_control real.launch.py camera:=l515 rtab:=true ur_type:=ur5e robot_ip:=192.168.1.170

or 

.. code-block:: python
    
    ros2 launch robot_control rtab.launch.py

Changing the configuration
^^^^^^^^^^^^^^^^^^^^^^^^^

If the user wants to change the variables in RTAB-Map, that has to be done in the rtab.launch.py file.

Parameters are what functions and variables that the package use and the remappings are where the information is gathered. For information as to what these parameters do, look at the `documentation <http://wiki.ros.org/rtabmap_slam>`_


.. code-block:: python

    parameters=[{
          'frame_id':'camera_link', # What frame it should track odom from (the loop closure graphics)
          'subscribe_scan_cloud':False, 
          'subscribe_depth':True,
          'subscribe_rgbd':False,
          'subscribe_odom_info':False, 
          'approx_sync':True,
          'queue_size':10, 
          'Kp/MaxFeatures': '-1', 
          'RGBD/LinearUpdate': '0', 
          'Icp/VoxelSize' : '0.05',
          'Icp/MaxCorrespondenceDistance' : '0.1',
          'wait_for_transform':1.0}]

Below, you can see the scan_cloud, which is the PointCloud topic, depth/image is a monochrome image with distance information, rgb image is self explainatory and info contains extrinsic and intrinsic camera information.

.. code-block:: python

    remappings=[
          ('scan_cloud', '/camera/depth/color/points'),
          ('depth/image', '/camera/aligned_depth_to_color/image_raw'),
          ('rgb/image', '/camera/color/image_raw'),
          ('rgb/camera_info', '/camera/color/camera_info')]
