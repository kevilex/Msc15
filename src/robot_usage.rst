Robot Usage
==================

The robot is controlled by using the robot_control package in conjunction with moveit_servo. 
If you are able to ping the robot and have the remote control program on the ur5, you are ready to use this package.


Applications
------------
There were made several applications in the master thesis, these are the ones that are available through the ros interface.

#. controller.py      - Translates the /joy message into twist and servoing commands for moveit_servo, also able to change controllers & reference frames (using the arrow buttons), preview- & follow trajectories and make waypoints for onthego (these functions should be split up). 
#. odom.py            - Sends odometry onto topic for rtabmap to use
#. preview_trajectory - Preview the trajectory that is made from control/points_output.txt and control/orientation_output.txt
#. follow_trajectory  - Follows the trajectory that is made from control/points_output.txt and control/orientation_output.txt
#. preview_onthego    - Preview the trajectory that is made from control/points_onthego.txt and control/orientation_onthego.txt
#. follow_onthego     - Preview the trajectory that is made from control/points_onthego.txt and control/orientation_onthego.txt

Launch files
------------
The robot_control package has launch files both for the simulation and the real robot. 
The only difference between these two is that the simulation launch file opens a docker container that simulates the interface with the robot.

The real.launch.py takes the following arguments:


#. ur_type in our case is the ur5e, can easily swap when using another ur robot
#. robot_ip is the same as set on the network tab on the tablet
#. launch_rviz is if you'd like to have the moveit rviz window or not
#. camera_arg is used to set the transform frame based on the different cameras (accepted args is d455 & l515), not directly usable in SD applications
#. rtab_arg is if one would like to launch rtabmap aswell


If the user launches the real launch file, the following things will be launched by default.

#. ur_robot_drivers - Used in order to be able to interface with the ur5 via ros
#. ur_moveit_launch - Used in order to use the robot drivers efficiently by using the MoveIt2 interface
#. controller.py    - Manages controller configuration for servoing.


If one launches rtabmap with the rtabmap argument, the following will be launched:

#. odometry_node    - Used in order to make rtabmap work
#. rtabmap_slam     - Used for mapping
#. rtabmap_viz      - Used to get a visual understanding of what the mapping algorithm is doing.


If the operator chooses a camera:

#. static_transform_publisher - publishes a static transform from the tool-end to the camera, based on the xyz & quaternion in the launch file

Example on how to launch the robot with all the arguments:

.. code-block:: bash
   
   ros2 launch robot_control real.launch.py ur_type:=ur5e robot_ip:=192.168.1.170 launch_rviz:=true camera:=d455 rtab:=false

Running trajectories
^^^^^^^^^^^^^^^^^^^
Trajectories are ran by utilizing robot_control follow_trajectory or follow_onthego. These are lists of xyz and vx, vy and vz in the trajectory space, but xyz qx, qy, qz, qw in onthego.

In order for the trajectories to be ran, the scaled_joint_trajectory_controller needs to be active. To check if it is active or not, use this command

.. code-block:: bash
   
   ros2 control list_controllers

In order to switch controllers one could use this command

.. code-block:: bash 

   ros2 control switch_controllers --activate scaled_joint_trajectory_controller

When the controller is up and running, one could launch trajectories with the moveit interface or from programs that are put in the correct file format.

Live servoing
^^^^^^^^^^^^

In order to live servo, one would have to use the forward_position_controller. Only one joint controller can be active at the time.

.. code-block:: bash 

   ros2 service call /servo_node/start_servo std_srvs/srv/Trigger
   ros2 control switch_controllers --activate forward_position_controller --deactivate scaled_joint_trajectory_controller

.. warning::

   If you have ran a trajectory and switch back to the forward_position_controller without using the servo start trigger service first, the robot will move as fast as possible to the last known position that it received from moveit_servo.