// ThreeScene.js
import React, { useRef, useEffect } from 'react';
import * as THREE from 'three';
import {PLYLoader} from './PLYLoader.js';
import { OrbitControls } from './OrbitControls.js';


function ThreeScene({ selectedPLY, showPointCloud, showMesh }) { 
  const mountRef = useRef(null);

  useEffect(() => {
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    mountRef.current.appendChild(renderer.domElement);
    
    // Adding orbiting controls and camera initial position
    const controls = new OrbitControls(camera, renderer.domElement);
    camera.position.set(0, 0, 3);


    // Clear existing scene objects first
    clearScene();

    // Adding ambient light
    const ambientLight = new THREE.AmbientLight(0x00ff00, 0.5);
    scene.add(ambientLight);

    // Instantiate the loader
    const loader = new PLYLoader();

    if (showPointCloud) {
      // Create and add point cloud to the scene
      loader.load(selectedPLY + 'points.ply', function (geometry) {
      
        // Create a material for the points
        const material = new THREE.PointsMaterial({
          size: 0.01, 
          color: 0xffffff, 
          vertexColors: true // Allows the use of vertex colors, if present in the PLY file.
        });
      
        // Create a Points object using the loaded geometry and material
        const pointCloud = new THREE.Points(geometry, material);

        // Rotate it properly so that the panning is correct
        pointCloud.rotation.x = -Math.PI/2;
        pointCloud.rotation.z = Math.PI/2;

      
        // Add the point cloud to the scene
        scene.add(pointCloud);
      });
    };

    if (showMesh) {
      loader.load((selectedPLY + 'mesh.ply'), function (geometry) {
        //const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 }); // Bright green
        const material = new THREE.MeshPhongMaterial({color: 0x00ff00});  
        const plyMesh = new THREE.Mesh(geometry, material);
        plyMesh.position.set(0,0,0);
        const scale = 1
        plyMesh.scale.set(scale,scale,scale);
        scene.add(plyMesh);
      });
      
    };
    
    
    const animate = function () {
      requestAnimationFrame(animate);
      renderer.render(scene, camera);
      controls.update();
    };

    function clearScene() {
      while(scene.children.length > 0){ 
        scene.remove(scene.children[0]); 
      }
    };

    animate();

    return () => {
      mountRef.current.removeChild(renderer.domElement);
      controls.dispose();
      clearScene();
    };
  }, [selectedPLY, showPointCloud, showMesh]);

  return <div ref={mountRef} className="three-scene-container" />;
}

export default ThreeScene;
