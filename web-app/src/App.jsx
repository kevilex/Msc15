import React, { useState, useEffect } from 'react';
import ThreeScene from './3d';
import Sidebar from './Sidebar';
import VisualizationControls from './controls';
import LoginForm from './Loginform'; 

import './App.css'; // Importing your main CSS for layout

function App() {


  // States for three
  const [selectedPLY, setSelectedPLY] = useState('ply/1/');
  const [showPointCloud, setShowPointCloud] = useState(false);
  const [showMesh, setShowMesh] = useState(false);
  
  // Selection for the sidebar
  const handleItemSelected = (item) => {
    setSelectedPLY(item.path);
    setShowPointCloud(false);
    setShowMesh(false);
  };

  // Items for the database
  const [items, setItems] = useState([]);

  // Asynchronous fetching of items from the database
  useEffect(() => {
    const fetchItems = async () => {
      try {
        const response = await fetch('http://localhost:5000/items');
        const rawData = await response.json();
  
        // Process items to group them by platform
        const itemsByPlatform = rawData.reduce((acc, item) => {
          // If the platform doesn't exist, initialize it
          if (!acc[item.platform]) {
            acc[item.platform] = {
              name: item.platform,
              imageSrc: item.imageSrc,
              path: item.path,
              subItems: []
            };
          }

          // Add the current item as a sub-item under its platform
          acc[item.platform].subItems.push({
            name: item.name,
            date: item.date, 
            path: item.path, 
          });
  
          return acc;
        }, {});
  
        // Convert the object back into an array
        const formattedItems = Object.values(itemsByPlatform);
        console.log(formattedItems);

        setItems(formattedItems);
      } catch (error) {
        console.error("Error fetching items:", error);
      }
    };
  
    fetchItems();
  }, []);


  // Login form
  const [user, setUser] = useState(null);
  const handleLoginSuccess = (username) => {
    setUser({ username });
  };



  return (
    <div>
      {!user ? (
        <LoginForm onLoginSuccess={handleLoginSuccess} />
      ) : (
      <div className="app">
        <Sidebar items={items} onSelectItem={handleItemSelected} />

        <VisualizationControls
          showPointCloud={showPointCloud}
          setShowPointCloud={setShowPointCloud}
          showMesh={showMesh}
          setShowMesh={setShowMesh}
        />

        <ThreeScene
          selectedPLY={selectedPLY} 
          showPointCloud={showPointCloud} 
          showMesh={showMesh}
        />
    </div>
      )}
  </div>
  );
}

export default App;