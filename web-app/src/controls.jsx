

function VisualizationControls({ showPointCloud, setShowPointCloud
                                , showMesh, setShowMesh }) {
  return (
    <div className="controls-container">
      <label>
        <input
          type="checkbox"
          checked={showPointCloud}
          onChange={(e) => setShowPointCloud(e.target.checked)}
        /> Show Point Cloud
      </label>
      <label>
        <input
          type="checkbox"
          checked={showMesh}
          onChange={(e) => setShowMesh(e.target.checked)}
        /> Show Mesh
      </label>
    </div>
  );
};

export default VisualizationControls;
