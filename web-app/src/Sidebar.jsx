// Sidebar.js
import React, { useState } from 'react';
import './App.css'; // Importing your main CSS for layout
import './Sidebar.css'

function Sidebar({ items, onSelectItem }) {

  // State to control the visibility of the sidebar
  const [isVisible, setIsVisible] = useState(true);

  // Function to toggle the sidebar visibility
  const toggleSidebar = () => {
    setIsVisible(!isVisible);
  };

  const handleHover = (e, index) => {
    const hoveredItem = e.currentTarget; // The element that was hovered
    const subSidebar = document.querySelector(`.sub-sidebar-${index}`); // Select the corresponding sub-sidebar
  
    if (subSidebar) {
      const rect = hoveredItem.getBoundingClientRect(); // Get the bounding rectangle of the hovered item
      const topPosition = rect.top + window.scrollY; // Calculate the top position, accounting for scrolling
      subSidebar.style.top = `${topPosition}px`; // Set the top style of the sub-sidebar
    }
  };

  return (
    <div>
      <div>
      {/* Toggle Button */}
      <button 
          onClick={toggleSidebar} 
          className="toggle-button"
        >
        <i class="fa-solid fa-bars"></i>
        {isVisible ? '' : ''}

      </button>
      </div>
      {/* Sidebar */}
      <div className={`sidebar ${isVisible ? 'sidebar-visible' : ''}`}>
        <h2>Platforms</h2>
        <ul>
        {items.map((item, index) => (
          <li key={index} 
              className="sidebar-item" 
              onMouseEnter={(e) => handleHover(e, index)} 
          >
            <img className="platform-pictures" src={`/${item.imageSrc}`} alt={item.name} />
            <span className="sidebar-item-text">{item.name}</span>
            {item.subItems && (
              <div className={`sub-sidebar sub-sidebar-${index}`}>
                {item.subItems.map((subItem, subIndex) => (
                  <div key={subIndex} 
                      className="sub-sidebar-item"
                      onClick={(e) => {
                        e.stopPropagation();
                        onSelectItem(subItem); // Selects the subItem when clicked
                      }}>
                    <div>{subItem.name}</div>
                    <div>{subItem.date}</div>
                  </div>
                ))}
              </div>
            )}
          </li>
        ))}
        </ul>
      </div>
    </div>
  );
}

export default Sidebar;