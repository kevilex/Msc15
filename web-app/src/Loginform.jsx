import React, { useState } from 'react';
import './Loginform.css';

function LoginForm({ onLoginSuccess }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();

    // Any credentials are accepted as long they are filled in
    if (username && password) {
      // Simulate successful login
      console.log('Logging in with:', username, password);
      onLoginSuccess(username); // Passing the username back to the App
    } else {
      alert('Please enter both username and password');
    }
  };

  return (
    <div className='bk'>
      <form onSubmit={handleSubmit} >
        <div className='parent'>
          <img className="logo" src={`logo.svg`} />

          <div className='textinputs'>
            <div>
              <input
                className='boxes'
                type="text"
                id="username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
                placeholder='username'
              />
            </div>
            <div>
              <input
                className='boxes'
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                placeholder='password'
              />
            </div>
          </div>
          <button className="loginbtn" type="submit"></button>
          <i class="fa-solid fa-arrow-right fa-lg"></i>
        </div>
      </form>
    </div>
  );
}

export default LoginForm;
