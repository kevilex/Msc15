#!/bin/bash
echo "1 for real manual, 2 for real trajectory, 3 for sim manual, 4 for sim trajectory"
read num
if [ $num = 1 ]; then
    terminator -l realjoy -p default
elif [ $num = 2 ]; then
    # Add your command for the case when num is 3
    terminator -l realtraj -p default
elif [ $num = 3 ]; then
    terminator -l simjoy -p default
elif [ $num = 4 ]; then
    terminator -l simitraj -p default
fi
