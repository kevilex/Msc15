import numpy as np

# Define paths to save points and orientations
path_point = "/home/kevin/workspacegl/Msc15/control/points_output.txt"
path_orientation = "/home/kevin/workspacegl/Msc15/control/orientation_output.txt"

# Parameters for the number eight
num_points = 30  # Number of points per circle
radius = 0.1  # Radius of each circle
x_offset = 0.5  # Offset in the X direction
center1 = (x_offset, 0.2, 0.5)  # Center of the top circle
center2 = (x_offset, -0.0, 0.5)  # Center of the bottom circle

# Generate points for the top circle (YZ plane)
theta1 = np.linspace(0, 2 * np.pi, num_points)
circle1 = [(center1[0], center1[1] + radius * np.cos(t), center1[2] + radius * np.sin(t)) for t in theta1]

# Generate points for the bottom circle (YZ plane)
theta2 = np.linspace(0, 2 * np.pi, num_points)
circle2 = [(center2[0], center2[1] + radius * np.cos(t), center2[2] + radius * np.sin(t)) for t in theta2]

# Generate points for the top circle (YZ plane)
theta1 = np.linspace(0, np.pi, num_points)  # First half of the circle
circle1_first_half = [(center1[0], center1[1] + radius * np.cos(t), center1[2] + radius * np.sin(t)) for t in theta1]
circle1_second_half = [(center1[0], center1[1] + radius * np.cos(t), center1[2] + radius * np.sin(t)) for t in -theta1]

# Generate points for the bottom circle (YZ plane)
theta2 = np.linspace(0, np.pi, num_points)  # First half of the circle
circle2_first_half = [(center2[0], center2[1] + radius * np.cos(t), center2[2] + radius * np.sin(t)) for t in theta2]
circle2_second_half = [(center2[0], center2[1] + radius * np.cos(t), center2[2] + radius * np.sin(t)) for t in -theta2]

# Combine the points
#points = circle1_second_half + circle1_first_half[::-1] + circle2_second_half + circle2_first_half[::-1]

points_hor =  circle2_first_half + circle2_second_half[::-1] + cir
cle1_first_half[::-1] + circle1_second_half + circle2_first_half + circle2_second_half[::-1] + circle1_first_half[::-1] + circle1_second_half

# Parameters for the number eight
num_points = 30  # Number of points per circle
radius = 0.1  # Radius of each circle
x_offset = 0.5  # Offset in the X direction
center1 = (x_offset, 0.1, 0.6)  # Center of the top circle
center2 = (x_offset, 0.1, 0.4)  # Center of the bottom circle


# Generate points for the left circle (XZ plane)
theta1 = np.linspace(0, 2 * np.pi, num_points)
circle_left = [(center1[0] + radius * np.cos(t), center1[1], center1[2] + radius * np.sin(t)) for t in theta1]

# Generate points for the right circle (XZ plane)
theta2 = np.linspace(0, 2 * np.pi, num_points)
circle_right = [(center2[0] + radius * np.cos(t), center2[1], center2[2] + radius * np.sin(t)) for t in theta2]

# Generate points for the top circle (XY plane)
theta_top = np.linspace(0, np.pi, num_points)  # First half of the circle
circle_top_first_half = [(center1[0], center1[1] + radius * np.sin(t), center1[2] + radius * np.cos(t)) for t in theta_top]
circle_top_second_half = [(center1[0], center1[1] + radius * np.sin(t), center1[2] + radius * np.cos(t)) for t in -theta_top]

# Generate points for the bottom circle (XY plane)
theta_bottom = np.linspace(0, np.pi, num_points)  # First half of the circle
circle_bottom_first_half = [(center2[0], center2[1] + radius * np.sin(t), center2[2] + radius * np.cos(t)) for t in theta_bottom]
circle_bottom_second_half = [(center2[0], center2[1] + radius * np.sin(t), center2[2] + radius * np.cos(t)) for t in -theta_bottom]

# Combine the points
points_ver =   circle_top_second_half[::-1] + circle_top_first_half + circle_bottom_second_half + circle_bottom_first_half[::-1] + circle_top_second_half[::-1] + circle_top_first_half + circle_bottom_second_half + circle_bottom_first_half[::-1]


points = points_hor + points_ver

# Generate orientation lists
orientation_x = [1] * len(points)
orientation_y = [0] * len(points)
orientation_z = [0] * len(points)

# Save the points to the points file
with open(path_point, 'w') as file:
    for point in points:
        file.write(f"{point[0]} {point[1]} {point[2]}\n")

print(f"Points saved to {path_point}")

# Save the orientations to the orientations file
with open(path_orientation, 'w') as file:
    for i in range(len(points)):
        file.write(f"{orientation_x[i]} {orientation_y[i]} {orientation_z[i]}\n")

print(f"Orientations saved to {path_orientation}")
